import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load('Configuration.StandardSequences.Services_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
#process.GlobalTag.globaltag = 'GR_R_42_V23::All'
process.GlobalTag.globaltag = 'START42_V17::All'
process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(1))
process.source = cms.Source('EmptySource')
process.demo = cms.EDAnalyzer('JetCorrectorDBReader',
  payloadName = cms.untracked.string('AK5PFchs'),
  #globalTag = cms.untracked.string('START42_V23'),
  #globalTag = cms.untracked.string('GR_R_42_V23'),
  globalTag = cms.untracked.string('START42_V17'),
  printScreen = cms.untracked.bool(False),
  createTextFile = cms.untracked.bool(True),
  useCondDB = cms.untracked.bool(True)
)
process.p = cms.Path(process.demo)
