#ifndef ELECTRONIDPRODUCER_H
#define ELECTRONIDPRODUCER_H

#include <string>
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"

namespace beag{
	class ElectronIDProducer{
		public:
			ElectronIDProducer();
			~ElectronIDProducer();

			bool passed(const pat::Electron *electron, std::string id_name);
		private:
			bool calculate_id(const pat::Electron *electron, std::string id_name);

			std::map<std::string, bool> direct_ids;
			std::map<std::string, bool> manual_ids;

			const static bool verbose = false;
	};
}

#endif
