#ifndef PTSORTER_H
#define PTSORTER_H

#include "Math/LorentzVector.h"
#include <vector>

template <class MyObject>
class PtSorter{
	public:
		void add(MyObject *p4);
		std::vector<MyObject*> get_sorted();
		void clear();

	private:
		static bool compare_pt(const MyObject *p4_1,
				       const MyObject *p4_2);
		std::vector<MyObject*> objects;
};

#include "../src/PtSorter.inl"

#endif
