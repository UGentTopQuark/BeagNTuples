#ifndef WDecayModeAnalyser_H
#define WDecayModeAnalyser_H

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include <map>
#include <string>

class WDecayModeAnalyser{
	public:
		// get_decay_mode(): 1 = e, 2 = mu, 3 = tau
		int get_decay_mode();
		void fill_events_from_collection(edm::Handle<reco::GenParticleCollection> part_collection);
		WDecayModeAnalyser();
		~WDecayModeAnalyser();
	private:	
		void reset_particles();
		bool process_daughters(const reco::GenParticle *current_particle, std::string shift_str);
		int decay_mode;

		static const bool verbose=false;
};
#endif
