#ifndef LEPTONCOLLECTION_H
#define LEPTONCOLLECTION_H

#include "Collection.h"

namespace beag{
	template<class CMSSWObject, class BeagObject>
	class LeptonCollection: public beag::Collection<CMSSWObject, BeagObject>{
		public:
			LeptonCollection(TTree *tree, TFile *outfile, std::string ident);
			~LeptonCollection();
			void set_trigger_object_finder(TriggerObjectFinder *object_finder);
			void set_aod_trigger_event(edm::Handle<trigger::TriggerEvent> aodTriggerEvent);
			void set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle);
			void set_trackbuilder_handle(edm::ESHandle<TransientTrackBuilder> track_builder, edm::Handle<std::vector<reco::Vertex> > pvertexHandle);
			void set_pfiso_conesizes(std::vector<std::string> *pfiso_conesizes_str);
		protected:
//			void match_trigger(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj);
			void write_gen_info(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj);

			beag::TriggerObjectFinder *trigger_obj_find;
			edm::Handle<reco::BeamSpot> beamSpotHandle;
			edm::Handle<std::vector<reco::Vertex> > pvertexHandle;
			edm::ESHandle<TransientTrackBuilder> track_builder;
			edm::Handle<trigger::TriggerEvent> aodTriggerEvent;

			std::vector<double> *pfiso_conesizes;
	};
}

#endif
