#ifndef BEAGTRIGGEROBJECTPRODUCER_H
#define BEAGTRIGGEROBJECTPRODUCER_H

#include "TTree.h"
#include "TFile.h"
#include <vector>
#include <string>
#include "BeagObjects/TriggerObject.h"
#include "DataFormats/Common/interface/View.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "TriggerObjectFinder.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/Common/interface/TriggerResults.h"

namespace beag{
	class TriggerObjectProducer{
		public:
			TriggerObjectProducer(TTree *tree, TFile *outfile, std::string ident);
			~TriggerObjectProducer();
			void fill_trigger_objects(edm::Handle<trigger::TriggerEvent> aodTriggerEvent);
			void set_trigger_object_finder(TriggerObjectFinder *object_finder);
			inline std::vector<beag::TriggerObject>* get_trigger_objects(){ return trigger_objects; };

		private:
			std::vector<beag::TriggerObject> *trigger_objects;
			
			edm::Handle<trigger::TriggerEvent> aodTriggerEvent;
			beag::TriggerObjectFinder *trigger_obj_find;
			
			TTree *tree;
			TFile *outfile;

                        static const bool verbose = false;
	};
}

#endif
