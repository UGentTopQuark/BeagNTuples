#ifndef BEAG_RECOGENMATCH_H
#define BEAG_RECOGENMATCH_H

#include "BeagObjects/TTbarGenEvent.h"
#include "BeagObjects/Muon.h"
#include "BeagObjects/Electron.h"
#include "BeagObjects/Jet.h"
#include "BeagObjects/Particle.h"
#include <vector>
#include <iostream>

namespace beag{
	class RecoGenMatch{
		public:
			RecoGenMatch();
			~RecoGenMatch();
			void set_gen_event(TTbarGenEvent *gen_event);
			template <class beagLepton>
			void match_leptons_to_particles(std::vector<beagLepton> *leptons);
			void match_jets_to_partons(std::vector<beag::Jet> *jets);
		private:
			bool jet_matches_gen_event_quark(beag::Jet &jet, beag::Jet &gen_event_jet, beag::Particle &gen_event_quark);
			TTbarGenEvent *gen_event;
	};
}

#endif
