#ifndef BEAGTRIGGEROBJECTFINDER_H
#define BEAGTRIGGEROBJECTFINDER_H

#include "TFile.h"
#include <vector>
#include <string>
#include "DataFormats/Common/interface/View.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/Common/interface/TriggerResults.h"

namespace beag{
	class TriggerObjectFinder{
		public:
			TriggerObjectFinder(TFile *outfile);
			~TriggerObjectFinder();
			void find_trigger_objects(edm::Handle<trigger::TriggerEvent> aodTriggerEvent,edm::Handle<edm::TriggerResults> HLTR, HLTConfigProvider *hltConfig);
			void find_l1seed_names();
			void set_triggers_to_write(std::vector<std::string> *triggers);
			void set_triggers_to_veto(std::vector<std::string> *veto_triggers);
			void set_hlt_config_provider(HLTConfigProvider *hltConfig);
			void set_trigger_menu(std::string trigger_menu);
			bool check_objects_found();
			std::map<int,std::vector<std::vector<bool> > > get_objects_to_write();
			std::map<int,std::vector<int> > get_keys_found();
			std::vector<std::string>* get_l1seed_names();
			void set_l1_from_l1extra(bool from_l1extra);
			inline void set_modules_for_trigger(std::vector<std::vector<std::string> > *modules){ this->modules_for_trigger = modules;};

		private:
			void get_trigger_objects(std::string module_name, std::string trigger_name ,int nbeag_trig, int nmodule);
                        std::vector<std::string> *triggers_to_write;
                        std::vector<std::vector<std::string> > *modules_for_trigger;
			std::map<std::string,bool> triggers_to_veto;

			std::map<int,std::vector<std::vector<bool> > >  objects_to_write;
			std::map<int,std::vector<int> > keys_found;
			
			TFile *outfile;

			HLTConfigProvider *hltConfig;
			edm::Handle<trigger::TriggerEvent> aodTriggerEvent;
			edm::Handle<edm::TriggerResults> HLTR;

			std::string trigger_menu;
			bool objects_found;
			bool l1_from_l1extra;

                        static const bool verbose = false;
	};
}

#endif
