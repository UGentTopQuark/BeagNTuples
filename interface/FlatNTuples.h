// -*- C++ -*-
//
// Package:    FlatNTuples
// Class:      FlatNTuples
// 
/**\class FlatNTuples FlatNTuples.cc analysers/FlatNTuples/src/FlatNTuples.cc

 Description: <one line class summary>

 Implementation:
     <Notes on implementation>
*/
//
// Original Author:  local user
//         Created:  Thu Dec  3 17:34:46 CET 2009
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/Registry.h"

#include "DataFormats/JetReco/interface/GenJet.h"

#include <SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h>

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"

#include "DataFormats/METReco/interface/CaloMET.h"
#include "DataFormats/METReco/interface/CaloMETCollection.h"
#include "DataFormats/METReco/interface/MET.h"
#include "DataFormats/METReco/interface/METCollection.h"
#include "DataFormats/METReco/interface/PFMET.h"
#include "DataFormats/METReco/interface/PFMETCollection.h"

#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h" 

//#include "MuonAnalysis/MuonAssociators/interface/PropagateToMuon.h"
//#include "HLTriggerOffline/Muon/interface/PropagateToMuon.h"

//#include "HLTriggerOffline/Muon/src/PropagateToMuon.cc" //needed 

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/Common/interface/TriggerResults.h"

#include <DataFormats/Common/interface/Ref.h>

#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include <map>

#include "DataFormats/Common/interface/View.h"
#include <string>

#include "BeagObjects/Jet.h"
#include "BeagObjects/Electron.h"
#include "BeagObjects/Muon.h"
#include "BeagObjects/MET.h"
#include "JetCollection.h"
#include "ConversionCollection.h"
#include "ElectronCollection.h"
#include "MuonCollection.h"
#include "METCollection.h"
#include "TriggerProducer.h"
#include "EventInformationProducer.h"
#include "TTbarGenEventProducer.h"
#include "PrimaryVertexProducer.h"
#include "PDFWeightProducer.h"
#include "RecoGenMatch.h"
#include "TriggerNameMapper.h"
#include "TagNameMapper.h"
#include "TriggerObjectFinder.h"
#include "TriggerObjectProducer.h"
#include "GenParticleProducer.h"
#include "L1ExtraObjectProducer.h"
#include "PartnerTrackFinder.h"
#include "ConvRej2012.h"
#include "GenMatch.h"

class FlatNTuples : public edm::EDAnalyzer {
   public:
      explicit FlatNTuples(const edm::ParameterSet&);
      ~FlatNTuples();


   private:
	virtual void beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup);
	virtual bool onRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup);
	void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&);
        virtual void analyze(const edm::Event&, const edm::EventSetup&);

	template <class JetType, class EType, class MuType, class METType>
	void initialise(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename beag::JetCollection<JetType> *&beag_jets, typename beag::ElectronCollection<EType> *&beag_electrons, typename beag::MuonCollection<MuType> *&beag_muons, typename beag::METCollection<METType> *&beag_mets);
	template <class EType>
	void initialise_electrons(typename beag::ElectronCollection<EType> *&beag_electrons, std::string e_id);
	template <class MuType>
	void initialise_muons(typename beag::MuonCollection<MuType> *&beag_muons, std::string mu_id);
	template <class JetType>
	void initialise_jets(typename beag::JetCollection<JetType> *&beag_jets,std::string jet_id);
	
	template <class JetType, class EType, class MuType, class METType>
	void fill_ntuples(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename beag::JetCollection<JetType> *&beag_jets, typename beag::ElectronCollection<EType> *&beag_electrons, typename beag::MuonCollection<MuType> *&beag_muons, typename beag::METCollection<METType> *&beag_mets);
	template <class EType>
	void fill_ntuple_electrons(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename beag::ElectronCollection<EType> *&beag_electrons, typename edm::Handle<edm::View<EType> > electronHandle);
	template <class MuType>
	void fill_ntuple_muons(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename beag::MuonCollection<MuType> *&beag_muons, typename edm::Handle<edm::View<MuType> > muonHandle);
	template <class JetType>
	void fill_ntuple_jets(const edm::Event& iEvent, const edm::EventSetup& iSetup,typename beag::JetCollection<JetType> *&beag_jets, typename edm::Handle<edm::View<JetType> > jetHandle);
	
      // ----------member data ---------------------------
        edm::InputTag eleLabel_;
        edm::InputTag muoLabel_;
        edm::InputTag jetLabel_;
        edm::InputTag jetIDLabel_;
        edm::InputTag tauLabel_;
        edm::InputTag metLabel_;
        edm::InputTag phoLabel_;
	edm::InputTag PriVertexLabel_;
	edm::InputTag hlTriggerResults_;
	edm::InputTag hltAodSummary_;
	//jl 04.02.11: add input tags for pf stuff
	edm::InputTag pfjetLabel_;
	edm::InputTag pfjetIDLabel_;
	edm::InputTag pfmetLabel_;
	edm::InputTag tcmetLabel_;
	edm::InputTag pfmetTypeILabel_;
	edm::InputTag pfmetTypeIILabel_;
	edm::InputTag gen_jetLabel_;
        edm::InputTag nonisomuoLabel_;
        edm::InputTag nomujetLabel_;
        edm::InputTag nonisoeleLabel_;
        edm::InputTag noejetLabel_;

	TFile *outfile;
	TTree *tree;
	TTree *trigger_name_mapping;
	TTree *tag_name_mapping;

	beag::ConversionCollection *beag_conv;
	beag::JetCollection<pat::Jet> *beag_jets_pat;
	beag::MuonCollection<pat::Muon> *beag_muons_pat;
	beag::ElectronCollection<pat::Electron> *beag_electrons_pat;
	beag::METCollection<pat::MET> *beag_mets_pat;

	beag::MuonCollection<reco::Muon> *beag_muons_reco;
	beag::ElectronCollection<reco::GsfElectron> *beag_electrons_reco;

	beag::JetCollection<reco::CaloJet> *beag_jets_calo;
	beag::METCollection<reco::CaloMET> *beag_mets_calo;

	beag::JetCollection<reco::PFJet> *beag_jets_pf;
	beag::METCollection<reco::MET> *beag_mets_pf;
        beag::METCollection<reco::MET> *beag_mets_tc;  //SC
        beag::METCollection<reco::MET> *beag_mets_pfTypeI;  
        beag::METCollection<reco::MET> *beag_mets_pfTypeII;  

	beag::JetCollection<pat::Jet> *beag_nomu_jets_pat;
	beag::MuonCollection<pat::Muon> *beag_noniso_muons_pat;
	beag::ElectronCollection<pat::Electron> *beag_noniso_electrons_pat;
	beag::JetCollection<pat::Jet> *beag_noe_jets_pat;

	beag::TriggerProducer *trigger_prod;
	beag::PrimaryVertexProducer *pvertex_prod;
	beag::TTbarGenEventProducer *gen_evt_prod;
	beag::RecoGenMatch *reco_gen_match;
	beag::EventInformationProducer *evt_info_prod;
	beag::TriggerNameMapper *trigger_name_mapper;
	beag::TriggerNameMapper *veto_trigger_name_mapper;
	beag::TriggerNameMapper *l1seed_name_mapper;
	beag::TagNameMapper *eID_mapper;
	beag::TagNameMapper *muID_mapper;
	beag::TagNameMapper *btag_mapper;
	beag::TagNameMapper *pdf_mapper;
	beag::TagNameMapper *pfiso_conesize_mapper;
	beag::TriggerObjectFinder *trigger_obj_find;
	beag::TriggerObjectProducer *trigger_obj_prod;
	beag::GenParticleProducer *gen_part_prod;
	beag::PDFWeightProducer *pdf_weight_prod;
	beag::L1ExtraObjectProducer *l1_extra_prod;
	beag::PartnerTrackFinder *conv_rej;
        beag::ConvRej2012 *conv_rej_2012;
	//beag::MuPropagator *mu_propagator;
	//PropagateToMuon *cms_mu_propagator;
	//edm::ParameterSet muon_propagator_cfg;

	HLTConfigProvider *hltConfig;

	double current_lumi_block;
	double prev_lumi_block;

	bool write_prescales;
	bool write_gen_particles;
        bool write_tt_gen_evt;
	bool write_pdf_event_weights;
	bool write_MET;
	bool l1_from_l1extra;
	//bool propagate_mu_to_station2;
	bool enable_partner_track_finder;
        bool enable_conv_rej_2012;
	bool write_loose_muons;
	bool write_loose_electrons;

	double gen_part_min_pt;
	double gen_part_max_eta;
	std::vector<int> selected_gen_part;

	double event_weight;

	std::string outfile_name;

	int prescale_set;

	GenMatch *gen_match;

	std::string trigger_menu;

	std::vector<std::string> *selected_triggers;
	std::vector<std::string> *vetoed_triggers;
	std::vector<std::string> *valid_triggers;
	std::vector<std::string> *selected_l1seeds;

	std::vector<std::string> *muonIDs;
	std::vector<std::string> *electronIDs;

	std::vector<std::string> *btag_algos;
	std::vector<std::string> triggers;
	std::vector<std::string> veto_triggers;
	std::vector<edm::InputTag> pdf_sets;
	std::vector<std::string> *pfiso_conesizes;

	edm::Handle<reco::JetIDValueMap> jetIDHandle;
	edm::Handle<reco::JetIDValueMap> pfjetIDHandle; //jl 04.02.11

	/***
	 * Handles used when filling lepton collections
	 ***/
	edm::Handle<reco::BeamSpot> beamSpotHandle;
	edm::ESHandle<TransientTrackBuilder> trackBuilder;
	edm::Handle<std::vector<reco::Vertex> > pvertexHandle;
	edm::Handle<trigger::TriggerEvent> aodTriggerEvent;	

	bool hltConfigChanged;

	bool is_monte_carlo;
	bool fill_trigger_objects;
	bool run_changed;
	bool lumi_block_changed;
	bool first_in_job;
	bool process_gen_jets;
	bool write_W_decay_info;

	std::string mJetCorServiceName;
	std::string pfmJetCorServiceName; //jl 04.02.11

	int processing_mode;
};
