#ifndef BEAG_TRIGGER_PRODUCER
#define BEAG_TRIGGER_PRODUCER

#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "BeagObjects/Trigger.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

namespace beag{
	class TriggerProducer{
		public:
			TriggerProducer(TTree *tree, TFile *outfile, std::string ident);
			~TriggerProducer();
			void set_triggers_to_write(std::vector<std::string> *triggers);
			void set_hlt_config_provider(HLTConfigProvider *hltConfig);
			void fill_trigger_information(edm::Handle<edm::TriggerResults> HLTR);
		private:
			std::vector<std::string> *triggers_to_write;
			std::vector<beag::Trigger> *trigger;
			TTree *tree;
			TFile *outfile;

			HLTConfigProvider *hltConfig;

			static const bool verbose = false;
			int temp_counter;
	};
}

#endif
