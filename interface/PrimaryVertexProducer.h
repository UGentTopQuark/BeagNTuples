#ifndef PRIMARYVERTEXPRODUCER_H
#define PRIMARYVERTEXPRODUCER_H

#include <vector>
#include "BeagObjects/PrimaryVertex.h"
#include "TTree.h"
#include "TFile.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

namespace beag{
	class PrimaryVertexProducer{
		public:
			PrimaryVertexProducer(TTree *tree, TFile *outfile, std::string ident);
			~PrimaryVertexProducer();

			void fill_primary_vertices(edm::Handle<std::vector<reco::Vertex> > vertex_handle);
		private:

			std::vector<beag::PrimaryVertex> *primary_vertices;
			TTree *tree;
			TFile *outfile;
	};
}

#endif
