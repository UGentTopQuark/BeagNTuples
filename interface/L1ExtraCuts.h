#ifndef L1EXTRACUTS_H
#define L1EXTRACUTS_H

namespace beag{
	class L1ExtraCuts{
		public:
			inline double get_min_pt(){ return min_pt; };
			inline unsigned int get_min_quality(){ return min_quality; };
	
			void set_min_pt(double min_pt);
			void set_min_quality(unsigned int min_quality);
		private:
			double min_pt;
			unsigned int min_quality;
	};
}

#endif
