#ifndef GENPARTICLEPRODUCER_H
#define GENPARTICLEPRODUCER_H

#include <vector>
#include "TFile.h"
#include "TTree.h"

#include "BeagObjects/GenParticle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "DataFormats/Common/interface/View.h"

namespace beag{
	class GenParticleProducer{
		public:
			GenParticleProducer(TTree *tree, TFile *outfile, std::string ident);
			~GenParticleProducer();

			void set_selected_particle_ids(std::vector<int> pdg_ids);
			void set_min_pt(double pt);
			void set_max_eta(double eta);

			void fill_particles(edm::Handle<reco::GenParticleCollection> part_collection);

		private:
			bool accept_particle(const reco::GenParticle *gen_part);
			beag::GenParticle create_beag_particle(const reco::GenParticle *gen_part);

			TTree *tree;
			TFile *outfile;

			double min_pt;
			double max_eta;

			const static bool verbose = false;

			std::vector<beag::GenParticle> *gen_particles;
			std::map<int, bool> particle_ids;
	};
}

#endif
