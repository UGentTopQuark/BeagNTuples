#ifndef BEAG_PDFWEIGHT_PRODUCER
#define BEAG_PDFWEIGHT_PRODUCER

#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "DataFormats/Common/interface/View.h"

namespace beag{
	class PDFWeightProducer{
		public:
			PDFWeightProducer(TTree *tree, TFile *outfile, std::string ident);
			~PDFWeightProducer();
			void fill_pdf_weights(edm::Handle<std::vector<double> > weightHandle, unsigned int pdf_index);
		private:
			TTree *tree;
			TFile *outfile;

			std::vector<std::vector<double> > *pdf_weights;

			static const bool verbose = false;
	};
}

#endif
