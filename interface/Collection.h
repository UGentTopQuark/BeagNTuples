#ifndef BEAG_COLLECTION_H
#define BEAG_COLLECTION_H

#include <DataFormats/Common/interface/Ref.h>
#include "DataFormats/Common/interface/View.h"
#include "TTree.h"
#include "TFile.h"
#include "TMath.h"
#include "Math/VectorUtil.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"

#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include <DataFormats/METReco/interface/CaloMET.h>
#include <DataFormats/METReco/interface/CaloMETCollection.h>
#include <DataFormats/METReco/interface/MET.h>
#include <DataFormats/METReco/interface/METCollection.h>
#include <DataFormats/METReco/interface/PFMET.h>
#include <DataFormats/METReco/interface/PFMETCollection.h>

#include "DataFormats/EgammaCandidates/interface/Conversion.h"

#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/IPTools/interface/IPTools.h"
#include "DataFormats/GeometryCommonDetAlgo/interface/Measurement1D.h"
#include "TriggerObjectFinder.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "BeagObjects/Jet.h"
#include "BeagObjects/Muon.h"
#include "BeagObjects/Electron.h"
#include "BeagObjects/MET.h"
#include "PtSorter.h"
#include "ElectronIDProducer.h"
#include "PartnerTrackFinder.h"
#include "ConvRej2012.h"

namespace beag{
	template<class CMSSWObject, class BeagObject>
	class Collection{
		public:
			Collection(TTree *tree, TFile *outfile, std::string ident);
			~Collection();
			virtual void fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle);
			void set_selection_list(std::vector<std::string> *selection_list);
			void set_tag_list(std::vector<std::string> *tag_list);
			void set_mc_matching(bool match_mc);
			std::vector<BeagObject>* get_beag_objects();
		protected:
			virtual void fill_common_vars(typename edm::View<CMSSWObject>::const_iterator handle, BeagObject &beag_obj);
			virtual void fill_special_vars(typename edm::View<CMSSWObject>::const_iterator handle, BeagObject &beag_obj)=0;

			virtual const char* get_type_string()=0;
			std::vector<BeagObject> *beag_objects;
			std::vector<std::string> *selected_items;	// list of selected variables written to output
									// eg. list of b-tag algorithms
			std::vector<std::string> *selected_tags;		// additional tags, eg LeptonID

			edm::Handle<edm::View<CMSSWObject> > cmssw_handle;	

			PtSorter<BeagObject> *pt_sorter;
			TTree *tree;
			TFile *outfile;
			bool mc_matching;
	};
}

#endif
