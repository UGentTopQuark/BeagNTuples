#ifndef BEAG_CONVERSION_H
#define BEAG_CONVERSION_H

#include "Particle.h"
#include <map>
#include <string>
#include <vector>

#include "Rtypes.h"

namespace beag{
	class Conversion{
		public:
			Conversion():nHitsMax_(99.),lxy_(0.),vtxProb_(0.), eleind_(-1){};
			virtual ~Conversion(){};

			inline void set_nHitsMax(double val){ nHitsMax_ = val; };
			inline void set_lxy(double val){ lxy_ = val; };
			inline void set_vtxProb(double val){ vtxProb_ = val; };
			inline void set_eleind(int val){ eleind_ = val; };

			inline double nHitsMax(){ return nHitsMax_; };
			inline double lxy(){ return lxy_; };
			inline double vtxProb(){ return vtxProb_; };
			inline int eleind(){ return eleind_; };

		private:
			double nHitsMax_;
			double lxy_;
			double vtxProb_;
			int eleind_;

		ClassDef(Conversion, 1);
	};
}

#endif
