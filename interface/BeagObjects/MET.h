#ifndef BEAG_MET_H
#define BEAG_MET_H

#include "Particle.h"
#include "Rtypes.h"

namespace beag{
	class MET: public Particle{
		public:
			virtual ~MET(){};

			double significance;

		ClassDef(MET, 1);
	};
}

#endif
