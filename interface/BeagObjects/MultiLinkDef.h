#ifdef __CINT__

#include "Electron.h"
#include "Conversion.h"
#include "EventInformation.h"
#include "Muon.h"
#include "Jet.h"
#include "Lepton.h"
#include "Particle.h"
#include "MET.h"
#include "Trigger.h"
#include "TriggerObject.h"
#include "TTbarGenEvent.h"
#include "PrimaryVertex.h"
#include "GenParticle.h"
#include <vector>

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ namespace beag;

#pragma link C++ class std::vector<std::vector<double> >+;
#pragma link C++ class std::vector<std::vector<string> >+;

#pragma link C++ class beag::Electron+;
#pragma link C++ class std::vector<beag::Electron>;

#pragma link C++ class beag::Muon+;
#pragma link C++ class std::vector<beag::Muon>;

#pragma link C++ class beag::Jet+;
#pragma link C++ class std::vector<beag::Jet>;

#pragma link C++ class beag::Lepton+;
#pragma link C++ class std::vector<beag::Lepton>;

#pragma link C++ class beag::Particle+;
#pragma link C++ class std::vector<beag::Particle>;

#pragma link C++ class beag::MET+;
#pragma link C++ class std::vector<beag::MET>;

#pragma link C++ class beag::Trigger+;
#pragma link C++ class std::vector<beag::Trigger>;

#pragma link C++ class beag::TriggerObject+;
#pragma link C++ class std::vector<beag::TriggerObject>;

#pragma link C++ class beag::TTbarGenEvent+;
#pragma link C++ class std::vector<beag::TTbarGenEvent>;

#pragma link C++ class beag::PrimaryVertex+;
#pragma link C++ class std::vector<beag::PrimaryVertex>;

#pragma link C++ class beag::GenParticle+;
#pragma link C++ class std::vector<beag::GenParticle>;

#pragma link C++ class beag::EventInformation+;
#pragma link C++ class std::vector<beag::EventInformation>;

#pragma link C++ class beag::Conversion;
#pragma link C++ class std::vector<beag::Conversion>;

#endif
