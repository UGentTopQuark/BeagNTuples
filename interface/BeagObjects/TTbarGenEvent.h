#ifndef BEAG_TTBARGENEVENT_H
#define BEAG_TTBARGENEVENT_H

#include "Particle.h"
#include "Jet.h"
#include "Lepton.h"

#include "Rtypes.h"

namespace beag{
	class TTbarGenEvent{
		public:
			TTbarGenEvent(): hadB_matched(0),
					lepB_matched(0),
					q_matched(0),
					qbar_matched(0),
					lep_matched(0),
					decay_channel(0){};
			virtual ~TTbarGenEvent(){};

			beag::Particle hadT, lepT, hadW, lepW, hadB, lepB, lep, neu, q, qbar, beamParton0, beamParton1;
			beag::Jet hadB_jet, lepB_jet, q_jet, qbar_jet;
			beag::Lepton reco_lep;
			int hadB_matched, lepB_matched, q_matched, qbar_matched, lep_matched;
			char decay_channel;	// 0: background
						// 1: semi-lept electron
						// 2: semi-lept muon
						// 3: ttbar other

		ClassDef(TTbarGenEvent, 1);
	};
}

#endif
