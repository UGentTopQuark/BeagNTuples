#ifndef BEAG_EVENTINFORMATION_H
#define BEAG_EVENTINFORMATION_H

#include "Rtypes.h"

#include <vector>

namespace beag{
	class EventInformation{
		public:
			EventInformation():lumi_block(0), event_number(0), run(0), cms_energy(0),
					trigger_changed(0), event_weight(1.), is_real_data(0), rhoFastJet(0.), rhoFastJetIso(0.),
					true_pu(0){};
			virtual ~EventInformation(){};

			double lumi_block;
			double event_number;
			double run;
			double cms_energy;

			int trigger_changed;

			double event_weight;

			int is_real_data;
			
			int W_decay_mode;

			double rhoFastJet;
			double rhoFastJetIso;

			double true_pu;

			std::vector<std::pair<int, double> > pu_info;
		
		ClassDef(EventInformation, 1);
	};
}

#endif
