#ifndef BEAG_BEAMSPOT_H
#define BEAG_BEAMSPOT_H

namespace beag{
	class BeamSpot{
		public:
			double x0, y0, z0;
			double width_x;
			double width_y;		// if set to -1: old beamspot definition
						// where only width is defined (written to width_x)
						// instead x and y width
	};
}

#endif
