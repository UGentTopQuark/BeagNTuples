#ifndef MUONCOLLECTION_H
#define MUONCOLLECTION_H

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "LeptonCollection.h"
//#include "MuonAnalysis/MuonAssociators/interface/PropagateToMuon.h"
//#include "HLTriggerOffline/Muon/interface/PropagateToMuon.h"
//#include "HLTriggerOffline/Muon/src/PropagateToMuon.cc"
#include "TrackingTools/TrajectoryState/interface/TrajectoryStateOnSurface.h"

namespace beag{
	template <class CMSSWObject>
	class MuonCollection: public beag::LeptonCollection<CMSSWObject, beag::Muon>{
		public:
			MuonCollection(TTree *tree, TFile *outfile, std::string ident);
			~MuonCollection();
			//void set_mu_propagator(beag::MuPropagator *mu_propagator);
			//inline void set_mu_propagator(PropagateToMuon *mu_propagator){ this->mu_propagator = mu_propagator; };
		private:
			void fill_muon_common(typename edm::View<CMSSWObject>::const_iterator cmssw_muon, beag::Muon &beag_muon);
			virtual void fill_special_vars(typename edm::View<CMSSWObject>::const_iterator handle, beag::Muon &beag_obj){};
			virtual const char* get_type_string();

			//beag::MuPropagator *beag_mu_propagator;
			//PropagateToMuon *mu_propagator;
	};
}

#endif
