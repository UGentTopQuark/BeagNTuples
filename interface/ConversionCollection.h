#ifndef CONVERSIONCOLLECTION_H
#define CONVERSIONCOLLECTION_H

#include "Collection.h"
#include <FWCore/Framework/interface/ESHandle.h>
#include "FWCore/Framework/interface/Event.h"
#include "RecoEgamma/EgammaTools/interface/ConversionTools.h"

#include "BeagObjects/Conversion.h"
#include "DataFormats/EgammaCandidates/interface/Conversion.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include <TMath.h>

namespace beag{
	class ConversionCollection{
		public:
			ConversionCollection(TTree *tree, TFile *outfile, std::string ident);
			~ConversionCollection();
			template <class CMSSWObject>
			void fill_collection(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename edm::Handle<edm::View<CMSSWObject> > hElectrons);
		private:
			TTree *tree;
			TFile *outfile;
			std::vector<beag::Conversion> *beag_objects;
	};
}

#endif
