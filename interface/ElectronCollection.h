#ifndef ELECTRONCOLLECTION_H
#define ELECTRONCOLLECTION_H

#include "DataFormats/PatCandidates/interface/Electron.h"
#include "Collection.h"
#include "LeptonCollection.h"
#include <FWCore/Framework/interface/ESHandle.h>
#include "RecoEcal/EgammaCoreTools/interface/EcalClusterLazyTools.h"
#include "FWCore/Framework/interface/Event.h"
#include "RecoEgamma/EgammaTools/interface/ConversionTools.h"

namespace beag{
	template<class CMSSWObject>
	class ElectronCollection: public beag::LeptonCollection<CMSSWObject, beag::Electron>{
		public:
			ElectronCollection(TTree *tree, TFile *outfile, std::string ident);
			~ElectronCollection();
			virtual void fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle, const edm::Event& iEvent, const edm::EventSetup& iSetup);
			void set_conv_rej(beag::PartnerTrackFinder *conv_rej);
			void set_conv_rej_2012(beag::ConvRej2012 *conv_rej_2012);
		private:
			virtual void fill_special_vars(typename edm::View<CMSSWObject>::const_iterator handle, beag::Electron &beag_obj, const edm::Event& iEvent, const edm::EventSetup& iSetup);
			inline virtual void fill_special_vars(typename edm::View<CMSSWObject>::const_iterator handle, beag::Electron &beag_obj){};
			void fill_electron_common(typename edm::View<CMSSWObject>::const_iterator cmssw_electron, beag::Electron &beag_electron, const edm::Event& iEvent, const edm::EventSetup& iSetup);
			virtual const char* get_type_string();

			int nelectron;

			beag::ElectronIDProducer *eid_producer;
			beag::PartnerTrackFinder *conv_rej;
			beag::ConvRej2012 *conv_rej_2012;
	};
}

#endif
