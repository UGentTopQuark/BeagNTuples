#ifndef GenMatch_H
#define GenMatch_H

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include <map>
#include <string>

class GenMatch{
	public:
		void print_evt_content();
		bool is_ok();
		bool tbar_decay_is_leptonic();
		bool t_decay_is_leptonic();
		// get_decay_mode(): 0 = fully hadronic, 1 = semi-leptonic, 2 = fully leptonic
		int get_decay_mode();
		reco::GenParticle* get_particle(std::string identifier);
		void fill_events_from_collection(edm::Handle<reco::GenParticleCollection> part_collection);
		void fill_events_from_MCatNLOcollection(edm::Handle<reco::GenParticleCollection> part_collection);
		static int get_top_id();
			/*
			 *	get_particle()
			 *	possible values for identifier:
			 *	t, tbar, b, bbar, Wplus, Wminus, Wplus_dp1, Wplus_dp2, Wminus_dp1, Wminus_dp2
			 *	(Wplus_dp1 == Wplus decay product 1)
			 *	dp1 always charged lepton / quark
			 *	dp2 always neutrino / anti-quark
			 */
		GenMatch();
		~GenMatch();
	private:	
		void reset_particles();
		const reco::GenParticle* get_daughter_self(const reco::GenParticle *current_particle);
		reco::GenParticle *t, *tbar, *b, *bbar, *Wplus, *Wminus, *q1, *qbar1, *q2, *qbar2,
		  *lepton1, *neutrino1, *lepton2, *neutrino2, *initial1, *initial2;
		int decay_mode; // 0 = fully hadronic, 1 = semi-lept, 2= full. leptonic
		bool found_t, found_tbar;
		bool t_leptonic, tbar_leptonic;

		static const bool verbose=false;
		static const int top_id;
};
#endif
