#ifndef BEAG_TTBARGENEVENTPRODUCER_H
#define BEAG_TTBARGENEVENTPRODUCER_H

#include "BeagObjects/TTbarGenEvent.h"
#include "GenMatch.h"
#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "DataFormats/Common/interface/View.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

namespace beag{
	class TTbarGenEventProducer{
		public:
			TTbarGenEventProducer(TTree *tree, TFile *outfile, std::string ident);
			~TTbarGenEventProducer();
			void fill_mc_information(edm::Handle<reco::GenParticleCollection> part_collection, const edm::Event & iEvent);
			beag::TTbarGenEvent* get_gen_event();
			inline void set_gen_match(GenMatch *gen_match) { this->gen_match = gen_match; };
		private:
			void fill_particle_p4(reco::GenParticle *reco_particle, beag::Particle &beag_particle);
			std::vector<beag::TTbarGenEvent> *gen_event;
			TTree *tree;
			TFile *outfile;
			GenMatch *gen_match;
	};
}

#endif
