#ifndef METCOLLECTION_H
#define METCOLLECTION_H

#include "DataFormats/PatCandidates/interface/MET.h"
#include "Collection.h"
#include "BeagObjects/MET.h"

namespace beag{
	template<class CMSSWObject>
	class METCollection: public beag::Collection<CMSSWObject, beag::MET>{
		public:
			METCollection(TTree *tree, TFile *outfile, std::string ident);
			~METCollection();
		private:
			virtual void fill_special_vars(typename edm::View<CMSSWObject>::const_iterator handle, beag::MET &beag_obj);
			virtual const char* get_type_string();
	};
}

#endif
