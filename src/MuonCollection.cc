#include "../interface/MuonCollection.h"

namespace beag{
template <class CMSSWObject>
beag::MuonCollection<CMSSWObject>::MuonCollection(TTree *tree, TFile *outfile, std::string ident) : beag::LeptonCollection<CMSSWObject, beag::Muon>(tree, outfile, ident)
{
	this->tree->Bronch(ident.c_str(),get_type_string(),&this->beag_objects);
}

template <class CMSSWObject>
beag::MuonCollection<CMSSWObject>::~MuonCollection()
{
}

template <class CMSSWObject>
void beag::MuonCollection<CMSSWObject>::fill_muon_common(typename edm::View<CMSSWObject>::const_iterator cmssw_muon, beag::Muon &beag_muon)
{
	beag_muon.ecal_iso = cmssw_muon->isolationR03().emEt;
	beag_muon.hcal_iso = cmssw_muon->isolationR03().hadEt;
	beag_muon.track_iso = cmssw_muon->isolationR03().sumPt;

	beag_muon.hcal_vcone = cmssw_muon->isolationR03().hadVetoEt;
	beag_muon.ecal_vcone = cmssw_muon->isolationR03().emVetoEt;

	beag_muon.nMatchedSegments = cmssw_muon->numberOfMatches(); 
	beag_muon.nMatchedStations = cmssw_muon->numberOfMatchedStations(); 
	beag_muon.vz = cmssw_muon->vz();

	// if lepton IDs set
	beag_muon.lepton_id = 0;

	beag_muon.charge = cmssw_muon->charge();
	beag_muon.isPFMuon = cmssw_muon->isPFMuon();

	reco::TrackRef track = cmssw_muon->innerTrack();
   
	if(!track.isNull() && track.isAvailable()){
		/*
		if(mu_propagator){
			TrajectoryStateOnSurface prop = mu_propagator->extrapolate(*track);
			if(prop.isValid()){
				beag_muon.station2_eta = prop.globalPosition().eta();
				beag_muon.station2_phi = prop.globalPosition().phi();
			}else{
				beag_muon.station2_eta = -99;
				beag_muon.station2_phi = -99;
			}
		}
	*/
		/*
			// Laria's implementation of the Muon propagation code
		if(beag_mu_propagator){
			beag_mu_propagator->propagate_track(track);
		}
		*/

		// old definition: z0 relative to beamspot
		if(this->beamSpotHandle.isValid()){
			reco::BeamSpot beamSpot=*(this->beamSpotHandle);
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
			beag_muon.z0 = track->dz(point);
			if(this->pvertexHandle->size() > 0){
				beag_muon.z0_pv = track->dz() - this->pvertexHandle->begin()->z();
			}
		}else
			std::cerr << "WARNING: beag::Collection<Muon>::fill_muon_common(): no valid beamspot" << std::endl;

		//d0 filled in fill_special_vars
		if(this->track_builder.isValid() ){
			// d0 from reco primary vertex
			reco::TransientTrack tt = this->track_builder->build(track);
			if(this->pvertexHandle->size() > 0){
				std::pair<bool,Measurement1D> ip = IPTools::absoluteTransverseImpactParameter(tt, *(this->pvertexHandle->begin()));
				beag_muon.d0_pv = ip.second.value();
				beag_muon.d0_sigma_pv = ip.second.error();
			}else{
				std::cerr << "WARNING: beag::Collection<Muon>::fill_muon_common(): no primary vertex in event, cannot calculate impact parameter significance wrt primary vertex" << std::endl;
			}
		}
		
		beag_muon.nPixelHits = track->hitPattern().numberOfValidPixelHits(); 
		beag_muon.nPixelLayers =  track->hitPattern().pixelLayersWithMeasurement(); 
		beag_muon.nTrackerHits = track->hitPattern().numberOfValidTrackerHits(); 
		beag_muon.nTrackerLayers =  track->hitPattern().trackerLayersWithMeasurement();
		beag_muon.trackerChi2 = track->normalizedChi2();
		beag_muon.nHits = track->numberOfValidHits();
		beag_muon.track_available = true;
	}else{
		beag_muon.track_available = false;
		//std::cerr << "WARNING: beag::Collection<Muon>::fill_special_vars(): no track available" << std::endl;
	}

	reco::TrackRef global_track = cmssw_muon->globalTrack();
	if(!global_track.isNull() && global_track.isAvailable()){
		//beag_muon.nHits =  cmssw_muon->numberOfValidHits(); //for trig sync ex.takes from inner track
		//beag_muon.nHits = global_track->numberOfValidHits();
		beag_muon.chi2 = global_track->normalizedChi2();
		beag_muon.ndf = global_track->ndof();
		beag_muon.global_chi2 = global_track->chi2();
		reco::HitPattern global_pattern = global_track->hitPattern();
		beag_muon.nMuonHits =  global_pattern.numberOfValidMuonHits(); 
		//beag_muon.nTrackerHits =  global_pattern.numberOfValidTrackerHits(); //inner track recommended 2011
		beag_muon.nLostTrackerHits =  global_pattern.numberOfLostTrackerHits(); //should this be global_track? 
		beag_muon.nStripHits =  global_pattern.numberOfValidStripHits();
		beag_muon.nLostStripHits =  global_pattern.numberOfLostStripHits(); 
		//Next part is to get nDT and CSC stations
		unsigned stationMask = cmssw_muon->stationMask();
		unsigned DTMASK = 0xF;
		unsigned CSCMASK = 0xF0;		
		unsigned DTbits = stationMask & DTMASK;
		unsigned CSCBits = (stationMask & CSCMASK) >> 4 ;
		int tmp_nDTStations = 0;
		int tmp_nCSCStations = 0;
		for (tmp_nDTStations = 0; DTbits > 0; DTbits >>= 1) {
			tmp_nDTStations += (DTbits & 0x1);
		}
		for (tmp_nCSCStations = 0; CSCBits > 0; CSCBits >>= 1) {
			tmp_nCSCStations += (CSCBits & 0x1);
		}
		beag_muon.nDTstations =  tmp_nDTStations; 
		beag_muon.nCSCstations =  tmp_nCSCStations; 

		beag_muon.track_available = true;

	}else{
		beag_muon.track_available = false;
		//std::cerr << "WARNING: beag::Collection<Muon>::fill_special_vars(): no global track available" << std::endl;
	}

	//match_trigger(cmssw_muon, beag_muon);
	if(this->mc_matching) this->write_gen_info(cmssw_muon, beag_muon);
}

template <>
void beag::MuonCollection<reco::Muon>::fill_special_vars(edm::View<reco::Muon>::const_iterator cmssw_muon, beag::Muon &beag_muon)
{
	fill_muon_common(cmssw_muon, beag_muon);

	reco::TrackRef track = cmssw_muon->innerTrack();
	if(!track.isNull() && track.isAvailable()){
		if(this->pvertexHandle->size() > 0){
			beag_muon.d0 = track->dxy(this->pvertexHandle->begin()->position()); //2012 MuPOG recommendation
		}
		beag_muon.d0_sigma = track->d0Error();//possibly nonsense
		//// old definition: d0 relative to beamspot
		//if(this->beamSpotHandle.isValid()){
		//	reco::BeamSpot beamSpot=*(this->beamSpotHandle);
		//	math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
		//
		//	beag_muon.d0 = -1.*track->dxy(point);
		//	//beag_muon.d0_sigma = sqrt( track->d0Error() * track->d0Error() + sqrt(beamSpot.BeamWidthX()*beamSpot.BeamWidthX()+beamSpot.BeamWidthY()*beamSpot.BeamWidthY()) );
		//	beag_muon.d0_sigma = track->d0Error();
		//}else
		//	std::cerr << "WARNING: beag::Collection<Muon>::fill_special_vars(): no valid beamspot" << std::endl;
	}

	// if lepton IDs set
	beag_muon.lepton_id = 0;

	if(this->selected_tags != NULL){
		unsigned long long nID=0;
		for(std::vector<std::string>::iterator muID = this->selected_tags->begin();
			muID != this->selected_tags->end();
			++muID){
			if(*muID == "AllGlobalMuons" && cmssw_muon->isGlobalMuon())
				beag_muon.lepton_id |= ((unsigned long long) 1 << nID);
			else if(*muID == "AllStandAloneMuons" && cmssw_muon->isStandAloneMuon())
				beag_muon.lepton_id |= ((unsigned long long) 1 << nID);
			else if(*muID == "AllTrackerMuons" && cmssw_muon->isTrackerMuon())
				beag_muon.lepton_id |= ((unsigned long long) 1 << nID);

			++nID;
		}
	}
}

template <>
void beag::MuonCollection<pat::Muon>::fill_special_vars(edm::View<pat::Muon>::const_iterator cmssw_muon, beag::Muon &beag_muon)
{
	fill_muon_common(cmssw_muon, beag_muon);

	beag_muon.d0 = cmssw_muon->dB();
	beag_muon.d0_sigma = cmssw_muon->edB();

	beag_muon.chargedHadronIso = cmssw_muon->chargedHadronIso();
	beag_muon.neutralHadronIso = cmssw_muon->neutralHadronIso();
	beag_muon.photonIso = cmssw_muon->photonIso();
	beag_muon.puChargedHadronIso = cmssw_muon->puChargedHadronIso();

	// if lepton IDs set
	beag_muon.lepton_id = 0;

	if(this->selected_tags != NULL){
		unsigned long long nID=0;
		for(std::vector<std::string>::iterator muID = this->selected_tags->begin();
			muID != this->selected_tags->end();
			++muID){
			if(cmssw_muon->muonID(*muID))
				beag_muon.lepton_id |= ((unsigned long long) 1 << nID);

			++nID;
		}
	}

	for(std::vector<double>::iterator conesize = pfiso_conesizes->begin();
	    conesize != pfiso_conesizes->end();conesize++){
		beag_muon.chargedHadronIsos.push_back(cmssw_muon->isoDeposit(pat::PfChargedHadronIso)->sumWithin(*conesize));
		beag_muon.neutralHadronIsos.push_back(cmssw_muon->isoDeposit(pat::PfNeutralHadronIso)->sumWithin(*conesize));
		beag_muon.photonIsos.push_back(cmssw_muon->isoDeposit(pat::PfGammaIso)->sumWithin(*conesize));
	}
}

template <class CMSSWObject>
const char* beag::MuonCollection<CMSSWObject>::get_type_string()
{
	return "std::vector<beag::Muon>";
}

template class beag::MuonCollection<reco::Muon>;
template class beag::MuonCollection<pat::Muon>;
}
