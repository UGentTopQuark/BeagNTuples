#include "../interface/TriggerNameMapper.h"

beag::TriggerNameMapper::TriggerNameMapper(TTree *tree, TFile *outfile, std::string ident, bool write_prescales, bool write)
{
	this->tree = tree;
	this->outfile = outfile;

	this->write_prescales = write_prescales;

	selected_triggers = new std::vector<std::string>();
	modules_for_trigger = new std::vector<std::vector<std::string> >();
	l1_prescales = NULL;
	hlt_prescales = NULL;
	if(write_prescales){
		l1_prescales = new std::vector<int>();
		hlt_prescales = new std::vector<int>();
	}
	if(write){
		tree->Bronch(ident.c_str(),"std::vector<std::string>",&selected_triggers);
		tree->Bronch((ident+"_modules").c_str(),"std::vector<std::vector<std::string> >",&modules_for_trigger);
	}
	if(write_prescales){
		tree->Bronch("l1_prescales","std::vector<unsigned int>",&l1_prescales);
		tree->Bronch("hlt_prescales","std::vector<unsigned int>",&hlt_prescales);
	}
}

beag::TriggerNameMapper::~TriggerNameMapper()
{
	if(selected_triggers){ delete selected_triggers; selected_triggers = NULL; }
	if(modules_for_trigger){ delete modules_for_trigger; modules_for_trigger = NULL; }
	if(l1_prescales){ delete l1_prescales; l1_prescales = NULL; }
	if(hlt_prescales){ delete hlt_prescales; hlt_prescales = NULL; }
}

void beag::TriggerNameMapper::set_trigger_expressions(std::vector<std::string> &trigger_expressions)
{
	this->trigger_expressions = trigger_expressions;
}

void beag::TriggerNameMapper::set_selected_triggers(std::vector<std::string> &selected_triggers)
{
	this->selected_triggers = &selected_triggers;
}

std::vector<std::string>* beag::TriggerNameMapper::get_selected_trigger_names(const edm::Event& iEvent, const edm::EventSetup& iSetup, std::vector<std::string> *valid_triggers, HLTConfigProvider *hltConfig, unsigned int prescale_set, bool to_be_written)
{
	if(verbose) std::cout << "getting selected trigger names..." << std::endl;
	selected_triggers->clear();
	modules_for_trigger->clear();
	if(write_prescales){
		l1_prescales->clear();
		hlt_prescales->clear();
	}

	boost::regex re;
	boost::cmatch matches;
	std::map<std::string,bool> already_selected_triggers;

	int nselected_triggers = 0;
	for(std::vector<std::string>::iterator trigger_expression = trigger_expressions.begin();
		trigger_expression != trigger_expressions.end();
		++trigger_expression){
		//re.assign(*trigger_expression, boost::regex_constants::icase);
		re.assign(*trigger_expression);
		if(verbose) std::cout << "testing for regex: " << *trigger_expression << std::endl;
		for(std::vector<std::string>::iterator valid_trigger = valid_triggers->begin();
			valid_trigger != valid_triggers->end();
			++valid_trigger){
		        bool matched = boost::regex_match(*valid_trigger, re);
		
			if(verbose) std::cout << "comparing with trigger: " << *valid_trigger << std::endl;
			
			if(matched){
				// make sure every trigger is added only once to the vector
				if(already_selected_triggers.find(*valid_trigger) == already_selected_triggers.end()){
					if(verbose) std::cout << "trigger " << *valid_trigger << " MATCHED regex " << *trigger_expression << std::endl;
					selected_triggers->push_back(*valid_trigger);

					std::vector<std::string> saved_tags = hltConfig->saveTagsModules(*valid_trigger);
					if(verbose) std::cout << "saved tags for trigger " << *valid_trigger << " size: " << saved_tags.size() << std::endl;

					modules_for_trigger->push_back(std::vector<std::string>());
                			for(std::vector<std::string>::iterator saved_tag = saved_tags.begin();
                			        saved_tag != saved_tags.end();
                			        ++saved_tag){
						(*modules_for_trigger)[(selected_triggers->size())-1].push_back(*saved_tag);
                			}
					if(write_prescales){
						std::pair<int, int> prescales = hltConfig->prescaleValues(iEvent,iSetup, *valid_trigger);	
						l1_prescales->push_back(prescales.first);
						hlt_prescales->push_back(prescales.second);
						if(verbose) std::cout << "l1 prescale value: " << prescales.first << std::endl;
						if(verbose) std::cout << "hlt prescale value: " << prescales.second << std::endl;
					}
					already_selected_triggers[*valid_trigger] = true;
					++nselected_triggers;
				}
			}
		}
	}

	return selected_triggers;
}
