#include "../interface/TagNameMapper.h"

beag::TagNameMapper::TagNameMapper(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	selected_tags = new std::vector<std::string>();
	tree->Bronch(ident.c_str(),"std::vector<std::string>",&selected_tags);
}

beag::TagNameMapper::~TagNameMapper()
{
	if(selected_tags){
		delete selected_tags;
		selected_tags = NULL;
	}
}

void beag::TagNameMapper::set_tag_names(std::vector<std::string> *tag_names)
{
	selected_tags->clear();
	*selected_tags = *tag_names;
}

void beag::TagNameMapper::set_tag_names(std::vector<edm::InputTag> *tag_names)
{
	selected_tags->clear();
	for(std::vector<edm::InputTag>::iterator tag_name = tag_names->begin(); 
		tag_name != tag_names->end();
		++tag_name){
		selected_tags->push_back(tag_name->instance());
	}
}
