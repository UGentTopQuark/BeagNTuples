#include "../interface/LeptonCollection.h"

namespace beag{
template <class CMSSWObject, class BeagObject>
beag::LeptonCollection<CMSSWObject, BeagObject>::LeptonCollection(TTree *tree, TFile *outfile, std::string ident) : beag::Collection<CMSSWObject, BeagObject>(tree, outfile, ident)
{
	pfiso_conesizes = new std::vector<double>();
}

template <class CMSSWObject, class BeagObject>
beag::LeptonCollection<CMSSWObject, BeagObject>::~LeptonCollection()
{
	if(pfiso_conesizes){delete pfiso_conesizes;pfiso_conesizes = NULL;}
}

/*
template <class CMSSWObject, class BeagObject>
void beag::LeptonCollection<CMSSWObject, BeagObject>::match_trigger(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
	if(trigger_obj_find == NULL){
		std::cerr << " ERROR: Collection: TriggerObjectFinder not set. Exiting." << std::endl;
		return;
	}
	
	if(!trigger_obj_find->check_objects_found()){
		std::cerr << "ERROR: Collection: TriggerObjectFinder find trigger objects not run. Exiting." << std::endl;
		return;
	}
	beag_obj.trigger = 0;
	const trigger::TriggerObjectCollection objects = aodTriggerEvent->getObjects();
	
	//map<key id, trigger id>
	std::map<int,std::vector<int> > keys_found = trigger_obj_find->get_keys_found();
	std::map<int,bool> trigger_matched;

	for(std::map<int,std::vector<int> >::iterator key = keys_found.begin(); key != keys_found.end();key++){
		if(trigger_matched.empty() || (trigger_matched.find((*key).first) == trigger_matched.end())){

			trigger::TriggerObject foundObject = objects[(*key).first];
			double deta = beag_obj.eta - foundObject.eta();
			double_t mpi = TMath::Pi();
			double dphi = beag_obj.phi - foundObject.phi();
			if ( dphi > mpi ) {
				dphi -= 2.0*mpi;
			} else if ( dphi <= -mpi ) {
				dphi += 2.0*mpi;
			}

			double dR = sqrt(deta*deta + dphi*dphi);
 			if(dR < 0.2){
				std::vector<int> nbeag_trigs = (*key).second;
				for(std::vector<int>::iterator nbeag_trig = nbeag_trigs.begin();
				    nbeag_trig != nbeag_trigs.end();nbeag_trig++){
					beag_obj.trigger |= ((unsigned long long) 1 << *nbeag_trig);
				}
				trigger_matched[(*key).first] = true;
			}
		}
	}
}
*/

template <>
void beag::LeptonCollection<reco::Muon, beag::Muon>::write_gen_info(edm::View<reco::Muon>::const_iterator cmssw_obj, beag::Muon &beag_obj)
{
}

template <>
void beag::LeptonCollection<reco::GsfElectron, beag::Electron>::write_gen_info(edm::View<reco::GsfElectron>::const_iterator cmssw_obj, beag::Electron &beag_obj)
{
}

template <class CMSSWObject, class BeagObject>
void beag::LeptonCollection<CMSSWObject, BeagObject>::write_gen_info(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
	// write information of initiating particle
	if(cmssw_obj->genParticle() != NULL){
		beag_obj.mc_eta = cmssw_obj->genParticle()->polarP4().eta();
		beag_obj.mc_phi = cmssw_obj->genParticle()->polarP4().phi();
		beag_obj.mc_mass = cmssw_obj->genParticle()->polarP4().mass();
		beag_obj.mc_pt = cmssw_obj->genParticle()->polarP4().pt();
		beag_obj.mc_id = cmssw_obj->genParticle()->pdgId();

		beag_obj.mc_matched = true;
	}else{
		beag_obj.mc_matched = false;
	}
}

template <class CMSSWObject, class BeagObject>
void beag::LeptonCollection<CMSSWObject, BeagObject>::set_trigger_object_finder(TriggerObjectFinder *object_finder)
{
	trigger_obj_find = object_finder;
}

template <class CMSSWObject, class BeagObject>
void beag::LeptonCollection<CMSSWObject, BeagObject>::set_aod_trigger_event(edm::Handle<trigger::TriggerEvent> aodTriggerEvent)
{
	this->aodTriggerEvent = aodTriggerEvent;
}

template <class CMSSWObject, class BeagObject>
void beag::LeptonCollection<CMSSWObject, BeagObject>::set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle)
{
	this->beamSpotHandle = beamSpotHandle;
}

template <class CMSSWObject, class BeagObject>
void beag::LeptonCollection<CMSSWObject, BeagObject>::set_trackbuilder_handle(edm::ESHandle<TransientTrackBuilder> track_builder, edm::Handle<std::vector<reco::Vertex> > pvertexHandle)
{
	this->track_builder = track_builder;
	this->pvertexHandle = pvertexHandle;
}

template <class CMSSWObject, class BeagObject>
void beag::LeptonCollection<CMSSWObject, BeagObject>::set_pfiso_conesizes(std::vector<std::string> *pfiso_conesizes_str)
{
	//Convert strings to double here. 
	//Otherwise you would have to do so per lepton per event.
	pfiso_conesizes->clear();
	for(std::vector<std::string>::iterator str = pfiso_conesizes_str->begin();
	    str != pfiso_conesizes_str->end(); str++){
		pfiso_conesizes->push_back(atof((*str).c_str()));
	}
}

template class beag::LeptonCollection<reco::Muon, beag::Muon>;
template class beag::LeptonCollection<pat::Muon, beag::Muon>;
template class beag::LeptonCollection<reco::GsfElectron, beag::Electron>;
template class beag::LeptonCollection<pat::Electron, beag::Electron>;
}
