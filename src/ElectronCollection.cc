#include "../interface/ElectronCollection.h"

namespace beag{

template <class CMSSWObject>
beag::ElectronCollection<CMSSWObject>::ElectronCollection(TTree *tree, TFile *outfile, std::string ident) : beag::LeptonCollection<CMSSWObject, beag::Electron>(tree, outfile, ident)
{
	eid_producer = new beag::ElectronIDProducer();
	conv_rej = NULL;
	conv_rej_2012 = NULL;
	this->tree->Bronch(ident.c_str(),get_type_string(),&this->beag_objects);
}

template <class CMSSWObject>
beag::ElectronCollection<CMSSWObject>::~ElectronCollection()
{
	if(eid_producer){
		delete eid_producer;
		eid_producer = NULL;
	}

	nelectron = 0;
}

template <class CMSSWObject>
void beag::ElectronCollection<CMSSWObject>::fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle, const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	this->outfile->cd();
	this->beag_objects->clear();

	this->cmssw_handle = handle;

	nelectron = 0;

	for(typename edm::View<CMSSWObject>::const_iterator cmssw_obj = handle->begin();
		cmssw_obj != handle->end();
		++cmssw_obj){

		beag::Electron *beag_obj = new beag::Electron();

		this->fill_common_vars(cmssw_obj, *beag_obj);
		fill_special_vars(cmssw_obj, *beag_obj, iEvent, iSetup);

		this->pt_sorter->add(beag_obj);
		++nelectron;
	}

	std::vector<beag::Electron*> tmp_objects = this->pt_sorter->get_sorted();

	for(typename std::vector<beag::Electron*>::iterator tmp_obj = tmp_objects.begin();
		tmp_obj != tmp_objects.end();
		++tmp_obj){
		this->beag_objects->push_back(**tmp_obj);
		delete *tmp_obj;
	}

	this->pt_sorter->clear();
}

template <class CMSSWObject>
void beag::ElectronCollection<CMSSWObject>::fill_electron_common(typename edm::View<CMSSWObject>::const_iterator cmssw_electron, beag::Electron &beag_electron,  const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	beag_electron.nelectron = nelectron;

	beag_electron.ecal_iso = cmssw_electron->dr03EcalRecHitSumEt();
	beag_electron.hcal_iso = cmssw_electron->dr03HcalTowerSumEt();
	beag_electron.track_iso = cmssw_electron->dr03TkSumPt();

	beag_electron.hcal_vcone = -1;
	beag_electron.ecal_vcone = -1;

	beag_electron.sc_eta = cmssw_electron->superCluster()->eta();
	beag_electron.sc_phi = cmssw_electron->superCluster()->phi();

	beag_electron.sigmaIetaIeta = cmssw_electron->sigmaIetaIeta();
	beag_electron.deltaPhiSuperClusterTrackAtVtx = cmssw_electron->deltaPhiSuperClusterTrackAtVtx();
	beag_electron.deltaEtaSuperClusterTrackAtVtx = cmssw_electron->deltaEtaSuperClusterTrackAtVtx();
	beag_electron.hadronicOverEm = cmssw_electron->hadronicOverEm();
	beag_electron.isEB = cmssw_electron->isEB();

	beag_electron.vz = cmssw_electron->vz();

	reco::GsfTrackRef track = cmssw_electron->gsfTrack();
	if(!track.isNull() && track.isAvailable()){
		if(this->beamSpotHandle.isValid()){
			reco::BeamSpot beamSpot=*(this->beamSpotHandle);
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());

			beag_electron.d0 = -1.*track->dxy(point);
			//beag_electron.d0_sigma = sqrt( track->d0Error() * track->d0Error() + sqrt(beamSpot.BeamWidthX()*beamSpot.BeamWidthX()+beamSpot.BeamWidthY()*beamSpot.BeamWidthY()) );
			beag_electron.d0_sigma = track->d0Error();
		}else
			std::cerr << "WARNING: beag::Collection<Electron>::fill_special_vars(): no valid beamspot" << std::endl;

		if(this->track_builder.isValid()){
			reco::TransientTrack tt = this->track_builder->build(track);
			if(this->pvertexHandle->size() > 0){
				std::pair<bool,Measurement1D> ip = IPTools::absoluteTransverseImpactParameter(tt, *(this->pvertexHandle->begin()));
				beag_electron.d0_pv = ip.second.value();
				beag_electron.d0_sigma_pv = ip.second.error();
			}else{
				std::cerr << "WARNING: beag::Collection<Electron>::fill_special_vars(): no primary vertex in event, cannot calculate impact parameter significance wrt primary vertex" << std::endl;
			}
		}

		beag_electron.nLostTrackerHits = track->trackerExpectedHitsInner().numberOfHits();

		beag_electron.track_available = true;
	}else{
		//std::cerr << "WARNING: beag::Collection<Electron>::fill_special_vars(): no track available" << std::endl;
		beag_electron.track_available = false;
	}

	//beag_electron.d0 = cmssw_electron->dB();
	//beag_electron.d0_sigma = cmssw_electron->edB();

	// if EnablePartnerTrackFiner = true in .py config
	if(conv_rej){
		reco::GsfElectron temp_e(*(dynamic_cast<const reco::GsfElectron*>(&*cmssw_electron)));
		conv_rej->calculate(&temp_e);
		beag_electron.dist = conv_rej->get_dist();
		beag_electron.dcot = conv_rej->get_dcot();
	}

        if(conv_rej_2012){
                reco::GsfElectron temp_e(*(dynamic_cast<const reco::GsfElectron*>(&*cmssw_electron)));
                conv_rej_2012->calculate(&temp_e);		
		beag_electron.passconversionveto = conv_rej_2012->pass_conv_veto();
	} 

	// if lepton IDs set
	beag_electron.lepton_id = 0;
/*	// moved to fill special var, needs to be fixed in EIDProducer for PAT eids->templates
	if(this->selected_tags != NULL){
		unsigned long long nID=0;
		for(std::vector<std::string>::iterator eID = this->selected_tags->begin();
			eID != this->selected_tags->end();
			++eID){
			bool eid = eid_producer->passed(dynamic_cast<const reco::GsfElectron*>(&*cmssw_electron), *eID);

			if(eid)
				beag_electron.lepton_id |= ((unsigned long long) 1 << nID);
			++nID;
		}
	}
*/

	edm::InputTag  reducedEBRecHitCollection(std::string("reducedEcalRecHitsEB"));
	edm::InputTag  reducedEERecHitCollection(std::string("reducedEcalRecHitsEE"));
	
	EcalClusterLazyTools myEcalCluster(iEvent, iSetup, reducedEBRecHitCollection, reducedEERecHitCollection);

	bool validKF= false;
	reco::TrackRef myTrackRef = cmssw_electron->closestCtfTrackRef();
	validKF = (myTrackRef.isAvailable());
	validKF = (myTrackRef.isNonnull());

	beag_electron.myMVAVar_fbrem           =  cmssw_electron->fbrem();
	beag_electron.myMVAVar_kfchi2          =  (validKF) ? myTrackRef->normalizedChi2() : 0 ;
	beag_electron.myMVAVar_kfhits          =  (validKF) ? myTrackRef->hitPattern().trackerLayersWithMeasurement() : -1. ; 
	beag_electron.myMVAVar_kfvalidhits          =  (validKF) ? myTrackRef->numberOfValidHits() : -1. ;   // for analysist save also this  
	beag_electron.myMVAVar_gsfchi2         =  cmssw_electron->gsfTrack()->normalizedChi2();  // to be checked 

	beag_electron.myMVAVar_deta            =  cmssw_electron->deltaEtaSuperClusterTrackAtVtx();
	beag_electron.myMVAVar_dphi            =  cmssw_electron->deltaPhiSuperClusterTrackAtVtx();
	beag_electron.myMVAVar_detacalo        =  cmssw_electron->deltaEtaSeedClusterTrackAtCalo();
	beag_electron.myMVAVar_dphicalo        =  cmssw_electron->deltaPhiSeedClusterTrackAtCalo();   

	beag_electron.myMVAVar_see             =  cmssw_electron->sigmaIetaIeta();    //EleSigmaIEtaIEta
	std::vector<float> vCov = myEcalCluster.localCovariances(*(cmssw_electron->superCluster()->seed())) ;
	if (!isnan(vCov[2])) beag_electron.myMVAVar_spp = sqrt (vCov[2]);   //EleSigmaIPhiIPhi
	else beag_electron.myMVAVar_spp = 0.;
	beag_electron.myMVAVar_etawidth        =  cmssw_electron->superCluster()->etaWidth();
	beag_electron.myMVAVar_phiwidth        =  cmssw_electron->superCluster()->phiWidth();
	beag_electron.myMVAVar_e1x5e5x5        =  (cmssw_electron->e5x5()) !=0. ? 1.-(cmssw_electron->e1x5()/cmssw_electron->e5x5()) : -1. ;
	beag_electron.myMVAVar_R9              =  myEcalCluster.e3x3(*(cmssw_electron->superCluster()->seed())) / cmssw_electron->superCluster()->rawEnergy();
	beag_electron.myMVAVar_nbrems          =  fabs(cmssw_electron->numberOfBrems());

	beag_electron.myMVAVar_HoE             =  cmssw_electron->hadronicOverEm();
	beag_electron.myMVAVar_EoP             =  cmssw_electron->eSuperClusterOverP();
	beag_electron.myMVAVar_IoEmIoP         =  (1.0/(cmssw_electron->ecalEnergy())) - (1.0 / cmssw_electron->p());  // in the future to be changed with cmssw_electron->gsfTrack()->p()
	beag_electron.myMVAVar_eleEoPout       =  cmssw_electron->eEleClusterOverPout();
	beag_electron.myMVAVar_EoPout          =  cmssw_electron->eSeedClusterOverPout();
	beag_electron.myMVAVar_PreShowerOverRaw=  cmssw_electron->superCluster()->preshowerEnergy() / cmssw_electron->superCluster()->rawEnergy();

	beag_electron.myMVAVar_eta             =  cmssw_electron->superCluster()->eta();         
	beag_electron.myMVAVar_pt              =  cmssw_electron->pt();

	reco::Vertex dummy;
	const reco::Vertex *pv = &dummy;
	if (this->pvertexHandle->size() != 0) {
	  pv = &*this->pvertexHandle->begin();
	} else { // create a dummy PV
	  reco::Vertex::Error e;
	  e(0, 0) = 0.0015 * 0.0015;
	  e(1, 1) = 0.0015 * 0.0015;
	  e(2, 2) = 15. * 15.;
	  reco::Vertex::Point p(0, 0, 0);
	  dummy = reco::Vertex(p, e, 0, 0, 0);
	}

	const reco::Vertex vertex(*pv);

	//d0
	if (cmssw_electron->gsfTrack().isNonnull()) {
	  beag_electron.myMVAVar_d0 = (-1.0)*cmssw_electron->gsfTrack()->dxy(pv->position()); 
	} else if (cmssw_electron->closestCtfTrackRef().isNonnull()) {
	  beag_electron.myMVAVar_d0 = (-1.0)*cmssw_electron->closestCtfTrackRef()->dxy(pv->position()); 
	} else {
	  beag_electron.myMVAVar_d0 = -9999.0;
	}
	
	//default values for IP3D
	beag_electron.myMVAVar_ip3d = -999.0; 
	// myMVAVar_ip3dSig = 0.0;
	if (cmssw_electron->gsfTrack().isNonnull()) {
		  const double gsfsign   = ( (-cmssw_electron->gsfTrack()->dxy(pv->position()))   >=0 ) ? 1. : -1.;
		  
		reco::GsfTrackRef track = cmssw_electron->gsfTrack();
		if(!track.isNull() && track.isAvailable()){
			//if(this->track_builder.isValid()){
		  		const reco::TransientTrack &tt = this->track_builder->build(cmssw_electron->gsfTrack()); 
		  		const std::pair<bool,Measurement1D> &ip3dpv =  IPTools::absoluteImpactParameter3D(tt,vertex);
		  		if (ip3dpv.first) {
		  		  double ip3d = gsfsign*ip3dpv.second.value();
		  		    //double ip3derr = ip3dpv.second.error();  
		  		  beag_electron.myMVAVar_ip3d = ip3d; 
		  		  // myMVAVar_ip3dSig = ip3d/ip3derr;
		  		}
			//}
			//else std::cerr << "track builder not valid" << std::endl;
		}
		else std::cerr << "track not valid" << std::endl;
	}

	beag_electron.charge = cmssw_electron->charge();

	//match_trigger(cmssw_electron, beag_electron);
	if(this->mc_matching) this->write_gen_info(cmssw_electron, beag_electron);
}

template <>
void beag::ElectronCollection<reco::GsfElectron>::fill_special_vars(edm::View<reco::GsfElectron>::const_iterator cmssw_electron, beag::Electron &beag_electron, const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	fill_electron_common(cmssw_electron, beag_electron, iEvent, iSetup);
}

template <>
void beag::ElectronCollection<pat::Electron>::fill_special_vars(edm::View<pat::Electron>::const_iterator cmssw_electron, beag::Electron &beag_electron, const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	fill_electron_common(cmssw_electron, beag_electron, iEvent, iSetup);

	beag_electron.chargedHadronIso = cmssw_electron->chargedHadronIso();
	beag_electron.neutralHadronIso = cmssw_electron->neutralHadronIso();
	beag_electron.photonIso = cmssw_electron->photonIso();
	beag_electron.puChargedHadronIso = cmssw_electron->puChargedHadronIso();

	beag_electron.myMVAVar_IoEmIoP         =  (1.0/(cmssw_electron->ecalEnergy())) - (1.0 / cmssw_electron->ecalDrivenMomentum().P());  // in the future to be changed with cmssw_electron->gsfTrack()->p()
	beag_electron.myMVAVar_pt              =  cmssw_electron->ecalDrivenMomentum().pt();

	if(this->selected_tags != NULL){
		unsigned long long nID=0;
		for(std::vector<std::string>::iterator eID = this->selected_tags->begin();
			eID != this->selected_tags->end();
			++eID){
			bool eid = eid_producer->passed(&*cmssw_electron, *eID);

//			if(eid)
//				std::cout << "id: " << *eID << " PASS" << std::endl; 
//			else
//				std::cout << "id: " << *eID << " fail" << std::endl;


			if(eid)
				beag_electron.lepton_id |= ((unsigned long long) 1 << nID);
			++nID;
		}
	}
	
	for(std::vector<double>::iterator conesize = pfiso_conesizes->begin();
	    conesize != pfiso_conesizes->end();conesize++){
		beag_electron.chargedHadronIsos.push_back(cmssw_electron->isoDeposit(pat::PfChargedHadronIso)->sumWithin(*conesize));
		beag_electron.neutralHadronIsos.push_back(cmssw_electron->isoDeposit(pat::PfNeutralHadronIso)->sumWithin(*conesize));
		beag_electron.photonIsos.push_back(cmssw_electron->isoDeposit(pat::PfGammaIso)->sumWithin(*conesize));
	}

}

template <class CMSSWObject>
const char* beag::ElectronCollection<CMSSWObject>::get_type_string()
{
	return "std::vector<beag::Electron>";
}

template <class CMSSWObject>
void beag::ElectronCollection<CMSSWObject>::set_conv_rej(beag::PartnerTrackFinder *conv_rej)
{
	this->conv_rej = conv_rej;
}

template <class CMSSWObject>
void beag::ElectronCollection<CMSSWObject>::set_conv_rej_2012(beag::ConvRej2012 *conv_rej_2012)
{
        this->conv_rej_2012 = conv_rej_2012;
}


template class beag::ElectronCollection<reco::GsfElectron>;
template class beag::ElectronCollection<pat::Electron>;

}	// end namespace beag
