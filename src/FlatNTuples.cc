#include "../interface/FlatNTuples.h"


FlatNTuples::FlatNTuples(const edm::ParameterSet& iConfig):
  eleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("electronTag")),
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  tauLabel_(iConfig.getUntrackedParameter<edm::InputTag>("tauTag")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  phoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("photonTag")),
  PriVertexLabel_(iConfig.getUntrackedParameter<edm::InputTag>("primaryVertexTag")),
  hlTriggerResults_ (iConfig.getParameter<edm::InputTag> ("HLTriggerResults")),
  hltAodSummary_ (iConfig.getParameter<edm::InputTag> ("HLTAodSummary")),
  pfjetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("pfjetTag")), //jl 04.02.11
  pfmetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("pfmetTag")),
  tcmetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("tcmetTag")), //SC
  pfmetTypeILabel_(iConfig.getUntrackedParameter<edm::InputTag>("pfmetTypeITag")), 
  pfmetTypeIILabel_(iConfig.getUntrackedParameter<edm::InputTag>("pfmetTypeIITag")),
  nonisomuoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("nonisomuonTag")),
  nomujetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("nomujetTag")),
  nonisoeleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("nonisoelectronTag")),
  noejetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("noejetTag"))
{
	gen_jetLabel_ = iConfig.getUntrackedParameter<edm::InputTag>("genjetTag");
	if(gen_jetLabel_.label() != "") process_gen_jets = true;
	else process_gen_jets = false;

	fill_trigger_objects = iConfig.getParameter<bool>("FillTriggerObjects");
	write_prescales = iConfig.getParameter<bool>("WriteTriggerPrescales");
	write_gen_particles = iConfig.getParameter<bool>("WriteGenParticles");
	//write_tt_gen_evt = iConfig.getParameter<bool>("WriteTTGenEvent");
	write_pdf_event_weights = iConfig.getParameter<bool>("WritePDFEventWeights");
	l1_from_l1extra = iConfig.getParameter<bool>("GetL1FromL1Extra");

	// No longer tested to work, needed to drop a large part of the code due to newer compiler issues, only relevant to certain trigger studies
	// for more info, see: https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideMuonAnalysisMuonAssociators
	//propagate_mu_to_station2 = iConfig.getParameter<bool>("PropagateMuToStation2"); 

	write_MET = iConfig.getParameter<bool>("WriteMET");
	enable_partner_track_finder = iConfig.getParameter<bool>("EnablePartnerTrackFinder");
        enable_conv_rej_2012 = iConfig.getParameter<bool>("EnableConvRej2012");

	jetIDLabel_ = iConfig.getUntrackedParameter<edm::InputTag>("jetIDTag");
	mJetCorServiceName = iConfig.getUntrackedParameter<std::string>("jetCorServiceName");

	//jl 04.02.11
	pfjetIDLabel_ = iConfig.getUntrackedParameter<edm::InputTag>("pfjetIDTag");
	pfmJetCorServiceName = iConfig.getUntrackedParameter<std::string>("pfjetCorServiceName");

	processing_mode = iConfig.getParameter<int>("ProcessingMode");

	pdf_sets = iConfig.getParameter<std::vector<edm::InputTag> >("PDFWeights");

	muonIDs = new std::vector<std::string>(iConfig.getParameter<std::vector<std::string> >("MuonIDs"));
	electronIDs = new std::vector<std::string>(iConfig.getParameter<std::vector<std::string> >("ElectronIDs"));

	outfile_name = iConfig.getParameter<std::string>("outfile");

	btag_algos = new std::vector<std::string>(iConfig.getParameter<std::vector<std::string> >("BTagAlgorithms"));

	pfiso_conesizes = new std::vector<std::string>(iConfig.getParameter<std::vector<std::string> >("LeptonPFIsoConeSizes"));

	//muon_propagator_cfg = iConfig.getParameter<edm::ParameterSet>("muon_propagator_cfg");

	triggers = iConfig.getParameter<std::vector<std::string> >("TriggerList");

	veto_triggers = iConfig.getParameter<std::vector<std::string> >("VetoObjectTriggers");

	gen_part_min_pt = iConfig.getParameter<double>("SelectedGenParticlesMinPt");
	gen_part_max_eta = iConfig.getParameter<double>("SelectedGenParticlesMaxEta");
	selected_gen_part = iConfig.getParameter<std::vector<int> >("SelectedGenParticles");

	write_W_decay_info = iConfig.getParameter<bool>("WriteWDecayInformation");

	write_loose_muons = iConfig.getParameter<bool>("WriteLooseMuons");
	write_loose_electrons = iConfig.getParameter<bool>("WriteLooseElectrons");

   	outfile = new TFile(outfile_name.c_str(),"RECREATE");
	outfile->cd();
   	tree = new TTree("tree","tree");

	current_lumi_block = -1;
	prev_lumi_block = -1;

	event_weight = 1.;

	hltConfigChanged = true;
	run_changed = true;
	lumi_block_changed = true;
	first_in_job = true;

	gen_part_prod = NULL;

	gen_match = NULL;

   	trigger_name_mapping = NULL;
   	tag_name_mapping = NULL;

	beag_jets_pat = NULL;
	beag_mets_pat = NULL;	
	beag_muons_pat = NULL;
	beag_electrons_pat = NULL;

	beag_muons_reco = NULL;
	beag_electrons_reco = NULL;

	beag_jets_calo = NULL;
	beag_mets_calo = NULL;

	beag_jets_pf = NULL;
	beag_mets_pf = NULL;

	beag_mets_tc      = NULL; //SC
	beag_mets_pfTypeI = NULL; 
	beag_mets_pfTypeII = NULL; 

	beag_nomu_jets_pat = 0;
	beag_noe_jets_pat = 0;
	beag_noniso_muons_pat = 0;
	beag_noniso_electrons_pat = 0;

	beag_conv = 0;

	trigger_prod = NULL;
       	trigger_obj_find = NULL;
	trigger_obj_prod = NULL;
	l1_extra_prod = NULL;

	evt_info_prod = NULL;
	pvertex_prod = NULL;
	trigger_name_mapper = NULL;
	veto_trigger_name_mapper = NULL;
	//l1seed_name_mapper = NULL;
	eID_mapper = NULL;
	muID_mapper = NULL;
	btag_mapper = NULL;
	pfiso_conesize_mapper = NULL;
	pdf_mapper = NULL;

	gen_evt_prod = NULL;
	reco_gen_match = NULL;
	pdf_weight_prod = NULL;
	valid_triggers = NULL;
	hltConfig = NULL;
	//cms_mu_propagator = NULL;
	conv_rej = NULL;
        conv_rej_2012 = NULL;
}

FlatNTuples::~FlatNTuples()
{
	if(gen_match){ delete gen_match; gen_match = NULL; }
	if(conv_rej){ delete conv_rej; conv_rej = NULL; }
        if(conv_rej_2012){ delete conv_rej_2012; conv_rej_2012 = NULL; } 
		//if(cms_mu_propagator){ delete cms_mu_propagator; cms_mu_propagator = NULL; }
		//if(mu_propagator){ delete mu_propagator; mu_propagator = NULL; }
	if(l1_extra_prod){ delete l1_extra_prod; l1_extra_prod = NULL; }
	if(pdf_mapper){ delete pdf_mapper; pdf_mapper = NULL; }
	if(gen_part_prod){ delete gen_part_prod; gen_part_prod = NULL; }
	if(btag_algos){ delete btag_algos; btag_algos = NULL; }
	if(pfiso_conesizes){ delete pfiso_conesizes; pfiso_conesizes = NULL; }
	if(valid_triggers){ delete valid_triggers; valid_triggers = NULL; }
	if(hltConfig){ delete hltConfig; hltConfig = NULL; }
	if(reco_gen_match){ delete reco_gen_match; reco_gen_match = NULL; }
	if(trigger_obj_find){ delete trigger_obj_find; trigger_obj_find = NULL; }
	if(trigger_obj_prod){ delete trigger_obj_prod; trigger_obj_prod = NULL; }
	if(trigger_prod){ delete trigger_prod; trigger_prod = NULL; }
	if(evt_info_prod){ delete evt_info_prod; evt_info_prod = NULL; }
	if(pdf_weight_prod){ delete pdf_weight_prod; pdf_weight_prod = NULL; }
	if(beag_conv){ delete beag_conv; beag_conv = 0; }
	if(beag_jets_pat){ delete beag_jets_pat; beag_jets_pat = NULL; }
	if(beag_electrons_pat){ delete beag_electrons_pat; beag_electrons_pat = NULL; }
	if(beag_muons_pat){ delete beag_muons_pat; beag_muons_pat = NULL; }
	if(beag_mets_pat){ delete beag_mets_pat; beag_mets_pat = NULL; }
	if(beag_noe_jets_pat){ delete beag_noe_jets_pat; beag_noe_jets_pat = NULL; }
	if(beag_nomu_jets_pat){ delete beag_nomu_jets_pat; beag_nomu_jets_pat = NULL; }
	if(beag_noniso_electrons_pat){ delete beag_noniso_electrons_pat; beag_noniso_electrons_pat = NULL; }
	if(beag_noniso_muons_pat){ delete beag_noniso_muons_pat; beag_noniso_muons_pat = NULL; }
   	if(outfile) outfile->cd();
   	if(tree){ tree->Write(); delete tree; tree = NULL; }
   	if(trigger_name_mapping){
   		trigger_name_mapping->Write();
   		delete trigger_name_mapping;
		trigger_name_mapping = NULL;
	}
   	if(outfile){
		outfile->Write();
		outfile->Close();
   		delete outfile;
		outfile = NULL;
	}
}


//
// member functions
//

template <class JetType, class EType, class MuType, class METType>
void FlatNTuples::fill_ntuples(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename beag::JetCollection<JetType> *&beag_jets, typename beag::ElectronCollection<EType> *&beag_electrons, typename beag::MuonCollection<MuType> *&beag_muons, typename beag::METCollection<METType> *&beag_mets)
{
       	edm::Handle<edm::View<EType> > electronHandle;
       	iEvent.getByLabel(eleLabel_,electronHandle);

       	edm::Handle<edm::View<MuType> > muonHandle;
       	iEvent.getByLabel(muoLabel_,muonHandle);

       	edm::Handle<edm::View<METType> > metHandle;
       	if(write_MET) iEvent.getByLabel(metLabel_,metHandle);

       	edm::Handle<edm::View<JetType> > jetHandle;
       	iEvent.getByLabel(jetLabel_,jetHandle);

       	edm::Handle<reco::TrackCollection> tracks_h;
       	iEvent.getByLabel("generalTracks",tracks_h);

       	edm::Handle<DcsStatusCollection> dcsHandle;
       	iEvent.getByLabel("scalersRawToDigi",dcsHandle);

        edm::Handle<reco::ConversionCollection> hConversions;
        iEvent.getByLabel("allConversions", hConversions);

	edm::Handle<std::vector< PileupSummaryInfo > >  PupInfo;

	if(first_in_job) initialise<JetType, EType, MuType, METType>(iEvent, iSetup, beag_jets, beag_electrons, beag_muons, beag_mets);

	if(is_monte_carlo){
		edm::InputTag PileupSrc_("addPileupInfo");
		iEvent.getByLabel(PileupSrc_, PupInfo);

		evt_info_prod->fill_pu_info(PupInfo);		
	}

	if(enable_partner_track_finder) conv_rej->set_handles(tracks_h, dcsHandle);
	if(enable_partner_track_finder) conv_rej->next_event(iSetup, !is_monte_carlo);
        if(enable_conv_rej_2012) conv_rej_2012->set_handles(beamSpotHandle, hConversions);
	/*
	 * event weights for pdfs
	 */
	if(write_pdf_event_weights){
		edm::Handle<std::vector<double> > weightHandle;

		unsigned int npdf = 0;
		for(std::vector<edm::InputTag>::iterator pdf_set = pdf_sets.begin();
			pdf_set != pdf_sets.end();
			++pdf_set){
			iEvent.getByLabel(*pdf_set, weightHandle);
			pdf_weight_prod->fill_pdf_weights(weightHandle, npdf);
			++npdf;
		}
	}

        edm::Handle<reco::GenParticleCollection> genParticles;

	edm::Handle<TtGenEvent> ttgenevent;

	outfile->cd();

	if(is_monte_carlo || write_gen_particles){
        	iEvent.getByLabel("genParticles", genParticles);
	}

	//if(is_monte_carlo || write_tt_gen_evt){
	//  iEvent.getByLabel("genEvent", ttgenevent);
	//}

	edm::Handle<edm::TriggerResults> HLTR;
	iEvent.getByLabel(hlTriggerResults_,HLTR);

	iEvent.getByLabel(hltAodSummary_, aodTriggerEvent); //global var

	edm::Handle< l1extra::L1MuonParticleCollection > l1muons;
	if(fill_trigger_objects && l1_from_l1extra){
		iEvent.getByLabel("l1extraParticles", l1muons);
	}

	iEvent.getByLabel("offlineBeamSpot", beamSpotHandle);//global var

	iSetup.get<TransientTrackRecord>().get("TransientTrackBuilder", trackBuilder);//global var

	iEvent.getByLabel(PriVertexLabel_, pvertexHandle);//global var

	if(lumi_block_changed){
		hltConfig->init(iEvent.getRun(), iSetup, trigger_menu, hltConfigChanged);
		if(write_prescales) prescale_set = hltConfig->prescaleSet(iEvent, iSetup);
			/*
			 *	- Prescale set must be updated if it changed.
			 *	  This can happen even if there is no
			 *	  hltConfigChange on the other hand,
			 *	- hltConfigChanges can only happen on run
			 *	  boundaries, don't check oon each lumi block
			 *	  boundary
			 */
	}

	// if hlt config changes, write out mapping between names and ids if necessary
	if(hltConfigChanged || first_in_job){
		valid_triggers->clear();
		*valid_triggers = hltConfig->triggerNames();
		std::cout << "----------" << std::endl;
		for(std::vector<std::string>::iterator valid_trigger = valid_triggers->begin();
			valid_trigger != valid_triggers->end();
			++valid_trigger){
			std::cout << *valid_trigger << std::endl;
		}
		std::cout << "----------" << std::endl;
		selected_triggers = trigger_name_mapper->get_selected_trigger_names(iEvent, iSetup, valid_triggers, hltConfig, prescale_set,true);
		std::cout << "==========" << std::endl;
		for(std::vector<std::string>::iterator valid_trigger = selected_triggers->begin();
			valid_trigger != selected_triggers->end();
			++valid_trigger){
			std::cout << *valid_trigger << std::endl;
		}
		std::cout << "==========" << std::endl;
		std::cout << "nselected triggers: " << selected_triggers->size() << std::endl;
		trigger_prod->set_triggers_to_write(selected_triggers);
		trigger_obj_find->set_modules_for_trigger(trigger_name_mapper->get_modules_for_trigger());
		vetoed_triggers = veto_trigger_name_mapper->get_selected_trigger_names(iEvent, iSetup, valid_triggers, hltConfig, false,false);
		trigger_obj_find->set_triggers_to_write(selected_triggers);
		trigger_obj_find->set_triggers_to_veto(vetoed_triggers);
		//selected_l1seeds = trigger_obj_find->get_l1seed_names();
		//l1seed_name_mapper->set_selected_triggers(*selected_l1seeds);
		trigger_name_mapping->Fill();
	}

	pvertex_prod->fill_primary_vertices(pvertexHandle);

	if(write_gen_particles) gen_part_prod->fill_particles(genParticles);

	fill_ntuple_jets<JetType>(iEvent, iSetup, beag_jets, jetHandle);
	fill_ntuple_muons<MuType>(iEvent, iSetup, beag_muons, muonHandle);
	fill_ntuple_electrons<EType>(iEvent, iSetup, beag_electrons, electronHandle);
	beag_conv->fill_collection<EType>(iEvent, iSetup, electronHandle);

	if(write_MET) beag_mets->fill_collection(metHandle);

	trigger_obj_find->find_trigger_objects(aodTriggerEvent,HLTR, hltConfig);
	//if(fill_trigger_objects && l1_from_l1extra && run_changed) l1_extra_prod->set_trigger_names(trigger_obj_find->get_l1seed_names());

	trigger_prod->fill_trigger_information(HLTR);

	if(fill_trigger_objects) trigger_obj_prod->set_trigger_object_finder(trigger_obj_find);
	if(fill_trigger_objects) trigger_obj_prod->fill_trigger_objects(aodTriggerEvent);
	if(fill_trigger_objects && l1_from_l1extra) l1_extra_prod->fill_l1_objects(l1muons);

	if(is_monte_carlo){
        	//edm::Handle<GenEventInfoProduct> generatorHandle;
        	//iEvent.getByLabel("generator",generatorHandle);
		//event_weight = generatorHandle->weight();

		edm::Handle<GenEventInfoProduct> evt_info;
		iEvent.getByType(evt_info);
		event_weight = evt_info->weight();

		gen_match->fill_events_from_collection(genParticles);
	}

	// rho fastjet for jet corrections
	edm::Handle<double> rhoH;
	iEvent.getByLabel(edm::InputTag("kt6PFJets","rho"),rhoH);
	double rhoFastJet = *(rhoH.product());

	// rho fastjet for lepton isolation (different eta range)
	edm::Handle<double> rhoHIso;
	iEvent.getByLabel(edm::InputTag("kt6PFJets","rho"),rhoHIso);
	double rhoFastJetIso = *(rhoHIso.product());

	evt_info_prod->fill_event_information(iEvent.run(), iEvent.luminosityBlock(), iEvent.id().event(), hltConfigChanged, -1, event_weight, !is_monte_carlo, rhoFastJet, rhoFastJetIso);
	if(is_monte_carlo && write_W_decay_info){ evt_info_prod->fill_W_decay_info(genParticles); }

	if(is_monte_carlo) gen_evt_prod->fill_mc_information(genParticles, iEvent);

	if(is_monte_carlo) reco_gen_match->set_gen_event(gen_evt_prod->get_gen_event());
	if(is_monte_carlo) reco_gen_match->match_jets_to_partons(beag_jets->get_beag_objects());
	if(is_monte_carlo) reco_gen_match->match_leptons_to_particles<beag::Muon>(beag_muons->get_beag_objects());
	if(is_monte_carlo) reco_gen_match->match_leptons_to_particles<beag::Electron>(beag_electrons->get_beag_objects());

	run_changed = false;
	lumi_block_changed = false;
	hltConfigChanged = false;
}

template <class EType>
void FlatNTuples::fill_ntuple_electrons(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename beag::ElectronCollection<EType> *&beag_electrons,typename edm::Handle<edm::View<EType> > electronHandle)
{
	beag_electrons->set_beamspot_handle(beamSpotHandle);
	beag_electrons->set_trackbuilder_handle(trackBuilder, pvertexHandle);
	beag_electrons->set_selection_list(selected_triggers);
	beag_electrons->set_trigger_object_finder(trigger_obj_find);
	beag_electrons->set_aod_trigger_event(aodTriggerEvent);
	beag_electrons->fill_collection(electronHandle, iEvent, iSetup);
	//reco gen match only run for central electron collection
}

template <class MuType>
void FlatNTuples::fill_ntuple_muons(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename beag::MuonCollection<MuType> *&beag_muons,typename edm::Handle<edm::View<MuType> > muonHandle)
{
	beag_muons->set_beamspot_handle(beamSpotHandle);
	beag_muons->set_trackbuilder_handle(trackBuilder, pvertexHandle);
	beag_muons->set_selection_list(selected_triggers);
	beag_muons->set_trigger_object_finder(trigger_obj_find);
	beag_muons->set_aod_trigger_event(aodTriggerEvent);
	beag_muons->fill_collection(muonHandle);
	//reco gen match only run for central muon collection
}

template <class JetType>
void FlatNTuples::fill_ntuple_jets(const edm::Event& iEvent, const edm::EventSetup& iSetup,typename beag::JetCollection<JetType> *&beag_jets,typename edm::Handle<edm::View<JetType> > jetHandle)
{
       	edm::Handle<edm::View<reco::GenJet> > gen_jetHandle;
        if(is_monte_carlo && process_gen_jets){
	                iEvent.getByLabel(gen_jetLabel_, gen_jetHandle);
	        }

	beag_jets->set_jetIDHandle(jetIDHandle);
	beag_jets->set_is_mc(is_monte_carlo);
	if(is_monte_carlo && process_gen_jets)
		beag_jets->set_genjetHandle(gen_jetHandle);
	beag_jets->set_jetCorrector(mJetCorServiceName, iSetup);
	beag_jets->fill_collection(jetHandle, iEvent);
	//reco gen match only run for central jet collection
}
// ------------ method called to for each event  ------------
void
FlatNTuples::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	using namespace edm;

	/*
	 *	definition of handles
	 * NOTE: fill central collections before filling any extra collections
	 * Some handles set in fill_ntuples.
	 */
	if(processing_mode == 0){		// PAT
		fill_ntuples<pat::Jet, pat::Electron, pat::Muon, pat::MET>(iEvent, iSetup, beag_jets_pat, beag_electrons_pat, beag_muons_pat, beag_mets_pat);

		if(write_loose_muons){
			if(first_in_job){
				initialise_jets<pat::Jet>(beag_nomu_jets_pat,"nomujets");
				initialise_muons<pat::Muon>(beag_noniso_muons_pat,"nonisomuons");
			}
			edm::Handle<edm::View<pat::Jet> > jetNoMuHandle;
			iEvent.getByLabel(nomujetLabel_,jetNoMuHandle);
			fill_ntuple_jets<pat::Jet>(iEvent, iSetup, beag_nomu_jets_pat, jetNoMuHandle);
			edm::Handle<edm::View<pat::Muon> > NonIsoMuHandle;
			iEvent.getByLabel(nonisomuoLabel_,NonIsoMuHandle);
			fill_ntuple_muons<pat::Muon>(iEvent, iSetup, beag_noniso_muons_pat,NonIsoMuHandle);
		}
		if(write_loose_electrons){
			if(first_in_job){
				initialise_jets<pat::Jet>(beag_noe_jets_pat,"noejets");
				initialise_electrons<pat::Electron>(beag_noniso_electrons_pat,"nonisoelectrons");
			}
			edm::Handle<edm::View<pat::Jet> > jetNoEHandle;
			iEvent.getByLabel(noejetLabel_,jetNoEHandle);
			fill_ntuple_jets<pat::Jet>(iEvent, iSetup, beag_noe_jets_pat, jetNoEHandle);
			edm::Handle<edm::View<pat::Electron> > NonIsoEHandle;
			iEvent.getByLabel(nonisoeleLabel_,NonIsoEHandle);
			fill_ntuple_electrons<pat::Electron>(iEvent, iSetup, beag_noniso_electrons_pat,NonIsoEHandle);
		}
	}else if(processing_mode == 1){		// AOD CALO
       		iEvent.getByLabel(jetIDLabel_,jetIDHandle);

		fill_ntuples<reco::CaloJet, reco::GsfElectron, reco::Muon, reco::CaloMET>(iEvent, iSetup, beag_jets_calo, beag_electrons_reco, beag_muons_reco, beag_mets_calo);
	}else if(processing_mode == 2){		// AOD PF
		fill_ntuples<reco::PFJet, reco::GsfElectron, reco::Muon, reco::MET>(iEvent, iSetup, beag_jets_pf, beag_electrons_reco, beag_muons_reco, beag_mets_pf);
	}
	else{
		std::cerr << "ERROR: invalid processing mode, exiting..." << std::endl;
		exit(1); 
	}
	tree->Fill(); 
	first_in_job = false;
}

template <class JetType, class EType, class MuType, class METType>
void FlatNTuples::initialise(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename beag::JetCollection<JetType> *&beag_jets, typename beag::ElectronCollection<EType> *&beag_electrons, typename beag::MuonCollection<MuType> *&beag_muons, typename beag::METCollection<METType> *&beag_mets)
{
	is_monte_carlo = !iEvent.isRealData();

	gen_part_prod = NULL;
	if(write_gen_particles){
		gen_part_prod = new beag::GenParticleProducer(tree, outfile, "gen_particles");
		gen_part_prod->set_min_pt(gen_part_min_pt);
		gen_part_prod->set_max_eta(gen_part_max_eta);
		gen_part_prod->set_selected_particle_ids(selected_gen_part);
	}

   	trigger_name_mapping = new TTree("trigger_name_mapping","trigger_name_mapping");
   	tag_name_mapping = new TTree("tag_name_mapping","tag_name_mapping");

	conv_rej = NULL;
	if(enable_partner_track_finder) conv_rej = new beag::PartnerTrackFinder();
       
	conv_rej_2012 = NULL;
	if(enable_conv_rej_2012) conv_rej_2012 = new beag::ConvRej2012();

	// Needed to be dropped for compiler issues, only relevant to certain trigger studies
	//cms_mu_propagator = NULL;
	//if(propagate_mu_to_station2){
	//	cms_mu_propagator = new PropagateToMuon(muon_propagator_cfg);
	//	cms_mu_propagator->init(iSetup);
	//}

	beag_conv = new beag::ConversionCollection(tree, outfile, "conversions");
	initialise_jets<JetType>(beag_jets,"jets");
	initialise_muons<MuType>(beag_muons,"muons");
	initialise_electrons<EType>(beag_electrons,"electrons");
	beag_mets = new beag::METCollection<METType>(tree, outfile, "mets");
	trigger_prod = new beag::TriggerProducer(tree, outfile, "trigger");
       	trigger_obj_find = new beag::TriggerObjectFinder(outfile);
	trigger_obj_prod = NULL;
	l1_extra_prod = NULL;
	if(fill_trigger_objects) trigger_obj_prod = new beag::TriggerObjectProducer(tree, outfile, "trigger_objects");
	if(fill_trigger_objects && l1_from_l1extra){
		l1_extra_prod = new beag::L1ExtraObjectProducer();
		l1_extra_prod->set_trigger_objects(trigger_obj_prod->get_trigger_objects());
		trigger_obj_find->set_l1_from_l1extra(true);
	}

	evt_info_prod = new beag::EventInformationProducer(tree, outfile, "evt_info");
	pvertex_prod = new beag::PrimaryVertexProducer(tree, outfile, "pvertices");
	trigger_name_mapper = new beag::TriggerNameMapper(trigger_name_mapping, outfile, "trigger_names", write_prescales, true);
	veto_trigger_name_mapper = new beag::TriggerNameMapper(trigger_name_mapping, outfile, "trigger_names", false, false);
	//l1seed_name_mapper = new beag::TriggerNameMapper(trigger_name_mapping, outfile, "l1seed_names", false, true);
	eID_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "eID_names");
	eID_mapper->set_tag_names(electronIDs);
	muID_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "muID_names");
	muID_mapper->set_tag_names(muonIDs);
	btag_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "btag_names");
	btag_mapper->set_tag_names(btag_algos);
        pfiso_conesize_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "pfiso_conesize_names");
	pfiso_conesize_mapper->set_tag_names(pfiso_conesizes);
	pdf_mapper = NULL;
	if(write_pdf_event_weights){
		pdf_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "pdf_names");
		pdf_mapper->set_tag_names(&pdf_sets);
	}
	// fill tag names only once at the beginning of job
	tag_name_mapping->Fill();

	gen_evt_prod = NULL;
	if(is_monte_carlo){
		gen_match = new GenMatch();
		gen_evt_prod = new beag::TTbarGenEventProducer(tree, outfile, "gen_evt");
		gen_evt_prod->set_gen_match(gen_match);
	}

	reco_gen_match = NULL;
	if(is_monte_carlo) reco_gen_match = new beag::RecoGenMatch();
	pdf_weight_prod = NULL;
	if(write_pdf_event_weights) pdf_weight_prod = new beag::PDFWeightProducer(tree, outfile, "pdf_evt_weights");
	//if(write_event_weights && !) evt_weight_prod = new beag::EventWeightProducer(tree, outfile, "evt_weights");

	valid_triggers = new std::vector<std::string>();

	hltConfig = new HLTConfigProvider();
	trigger_menu = hlTriggerResults_.process();
	trigger_prod->set_hlt_config_provider(hltConfig);
	trigger_name_mapper->set_trigger_expressions(triggers);
	trigger_obj_find->set_hlt_config_provider(hltConfig);
	trigger_obj_find->set_trigger_menu(trigger_menu);

	// Needed to be dropped for compiler issues, only relevant to certain trigger studies
	//mu_propagator = NULL;
	/*	// Laria's propagtion code
		// If re-implementing move to before initialise_muons
	if(propagate_mu_to_station2){
		mu_propagator = new beag::MuPropagator();
		beag_muons->set_mu_propagator(mu_propagator);
		mu_propagator->set_event_setup(iSetup);
	}
	*/

	veto_trigger_name_mapper->set_trigger_expressions(veto_triggers);
}
template <class EType>
void FlatNTuples::initialise_electrons(typename beag::ElectronCollection<EType> *&beag_electrons, std::string e_id)
{
	beag_electrons = new beag::ElectronCollection<EType>(tree, outfile, e_id);
	beag_electrons->set_tag_list(electronIDs);
	beag_electrons->set_pfiso_conesizes(pfiso_conesizes);
	beag_electrons->set_mc_matching(is_monte_carlo);
	beag_electrons->set_conv_rej(conv_rej);
        beag_electrons->set_conv_rej_2012(conv_rej_2012);
}
template <class MuType>
void FlatNTuples::initialise_muons(typename beag::MuonCollection<MuType> *&beag_muons, std::string mu_id)
{
	beag_muons = new beag::MuonCollection<MuType>(tree, outfile, mu_id);
	beag_muons->set_tag_list(muonIDs);
	beag_muons->set_pfiso_conesizes(pfiso_conesizes);
	beag_muons->set_mc_matching(is_monte_carlo);
	//	if(propagate_mu_to_station2){
	//	beag_muons->set_mu_propagator(cms_mu_propagator);
	//} 
}
template <class JetType>
void FlatNTuples::initialise_jets(typename beag::JetCollection<JetType> *&beag_jets, std::string jet_id)
{
	beag_jets = new beag::JetCollection<JetType>(tree, outfile, jet_id);
	beag_jets->set_selection_list(btag_algos);
}

void FlatNTuples::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
	lumi_block_changed = true;
}

bool FlatNTuples::onRun(edm::Run const &run, edm::EventSetup const &setup)
{
	if(hlTriggerResults_.process() == "")
	{
		std::cout << "hlTriggerResults_ is empty -> trying to determine the process name automatically:" << std::endl;
		edm::Handle<trigger::TriggerEvent> tmpTriggerEventHLT;

		const edm::ProcessHistory& processHistory(run.processHistory());
		for (edm::ProcessHistory::const_iterator it = processHistory.begin(); it != processHistory.end(); ++it)
		{
			std::cout << "\t" << it->processName() << std::endl;
			edm::ProcessConfiguration processConfiguration;
			if (processHistory.getConfigurationForProcess(it->processName(),processConfiguration))
			{
				edm::ParameterSet processPSet;
				if (edm::pset::Registry::instance()->getMapped(processConfiguration.parameterSetID(), processPSet))
				{
					if (processPSet.exists("hltTriggerSummaryAOD")){
						hlTriggerResults_ = edm::InputTag(hlTriggerResults_.label(), "", it->processName());
						trigger_menu = it->processName();
						hltAodSummary_ = edm::InputTag(hltAodSummary_.label(), "", it->processName());
					}
				}
			}
		}
		std::cout << "selected:" << hlTriggerResults_ << std::endl;
		if (hlTriggerResults_.process() == "")
			return false;
	}

	return true;
}

void FlatNTuples::beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup)
{
	run_changed = true;
	/* // Laria's muon propagator
	if(mu_propagator && propagate_mu_to_station2)
		mu_propagator->set_event_setup(currentEventSetup);
	*/

	//if(cms_mu_propagator && propagate_mu_to_station2)
	//	cms_mu_propagator->init(currentEventSetup);
	
	bool found_menu = onRun(currentRun,currentEventSetup);
	if(!found_menu)
		std::cerr << "ERROR: No trigger menu found in onRun()" << std::endl; 
}

//define this as a plug-in
DEFINE_FWK_MODULE(FlatNTuples);
