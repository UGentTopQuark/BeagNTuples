#include "../interface/ElectronIDProducer.h"

beag::ElectronIDProducer::ElectronIDProducer()
{
	// define ids that are taken from the PAT information
	direct_ids["eidRobustTight"] = true;
	direct_ids["eidRobustLoose"] = true;
	direct_ids["eidTight"] = true;
	direct_ids["eidLoose"] = true;
	direct_ids["eidVeryLooseMC"] = true;
	direct_ids["eidLooseMC"] = true;
	direct_ids["eidMediumMC"] = true;
	direct_ids["eidTightMC"] = true;
	direct_ids["eidSuperTightMC"] = true;
	direct_ids["eidHyperTightMC"] = true;
	direct_ids["eidHyperTight1MC"] = true;
	direct_ids["eidHyperTight2MC"] = true;
	direct_ids["eidHyperTight3MC"] = true;
	direct_ids["eidHyperTight4MC"] = true;
	direct_ids["simpleEleId95relIso"] = true;
	direct_ids["simpleEleId90relIso"] = true;
	direct_ids["simpleEleId85relIso"] = true;
	direct_ids["simpleEleId80relIso"] = true;
	direct_ids["simpleEleId70relIso"] = true;
	direct_ids["simpleEleId60relIso"] = true;
	direct_ids["simpleEleId95cIso"] = true;
	direct_ids["simpleEleId90cIso"] = true;
	direct_ids["simpleEleId85cIso"] = true;
	direct_ids["simpleEleId80cIso"] = true;
	direct_ids["simpleEleId70cIso"] = true;
	direct_ids["simpleEleId60cIso"] = true;
	direct_ids["eidRobustTight_eID_only"] = true;
	direct_ids["eidRobustLoose_eID_only"] = true;
	direct_ids["eidTight_eID_only"] = true;
	direct_ids["eidLoose_eID_only"] = true;
	direct_ids["eidVeryLooseMC_eID_only"] = true;
	direct_ids["eidLooseMC_eID_only"] = true;
	direct_ids["eidMediumMC_eID_only"] = true;
	direct_ids["eidTightMC_eID_only"] = true;
	direct_ids["eidSuperTightMC_eID_only"] = true;
	direct_ids["eidHyperTightMC_eID_only"] = true;
	direct_ids["eidHyperTight1MC_eID_only"] = true;
	direct_ids["eidHyperTight2MC_eID_only"] = true;
	direct_ids["eidHyperTight3MC_eID_only"] = true;
	direct_ids["eidHyperTight4MC_eID_only"] = true;
	direct_ids["simpleEleId95relIso_eID_only"] = true;
	direct_ids["simpleEleId90relIso_eID_only"] = true;
	direct_ids["simpleEleId85relIso_eID_only"] = true;
	direct_ids["simpleEleId80relIso_eID_only"] = true;
	direct_ids["simpleEleId70relIso_eID_only"] = true;
	direct_ids["simpleEleId60relIso_eID_only"] = true;
	direct_ids["simpleEleId95cIso_eID_only"] = true;
	direct_ids["simpleEleId90cIso_eID_only"] = true;
	direct_ids["simpleEleId85cIso_eID_only"] = true;
	direct_ids["simpleEleId80cIso_eID_only"] = true;
	direct_ids["simpleEleId70cIso_eID_only"] = true;
	direct_ids["simpleEleId60cIso_eID_only"] = true;

	direct_ids["eidVeryLooseMC_no_iso"] = true;
	direct_ids["eidLooseMC_no_iso"] = true;
	direct_ids["eidMediumMC_no_iso"] = true;
	direct_ids["eidTightMC_no_iso"] = true;
	direct_ids["eidSuperTightMC_no_iso"] = true;
	direct_ids["eidHyperTightMC_no_iso"] = true;
	direct_ids["eidHyperTight1MC_no_iso"] = true;
	direct_ids["eidHyperTight2MC_no_iso"] = true;
	direct_ids["eidHyperTight3MC_no_iso"] = true;
	direct_ids["eidHyperTight4MC_no_iso"] = true;

	// define ids that need to be calculated
	manual_ids["simpleEleId70Run2010"] = true;
	manual_ids["simpleEleId95Run2010"] = true;
	manual_ids["simpleEleId70Run2011"] = true;
	manual_ids["simpleEleId95Run2011"] = true;
}

beag::ElectronIDProducer::~ElectronIDProducer()
{
}

bool beag::ElectronIDProducer::passed(const pat::Electron *electron, std::string id_name)
{
	if(direct_ids.find(id_name) != direct_ids.end()){
		bool passes_id = false;
		if(id_name.find("_eID_only") != std::string::npos){

			// FIXME
			// remove _eID_only part of ident. this needs
			// to be changed and cleaned up in the future
			size_t start_pos = id_name.rfind("_eID_only");
			if(start_pos != std::string::npos)
				id_name.replace(start_pos,9,"");

			if(electron->isElectronIDAvailable(id_name)){
				/*
				 * NOTE:
				 * remember that when checking bit patterns the shift of 0 (!) indicates the first bit
				 * 1 << 0: check 1000000000000000000000000000000000000000
				 * 1 << 1: check 0100000000000000000000000000000000000000
				 * 1 << 2: check 0010000000000000000000000000000000000000
				 * and so on...
				 *
				 */
				if(((unsigned long long) electron->electronID(id_name) & ((unsigned long long) 1 << (unsigned long long) 0) ))	// passes eID part
					passes_id = true;
			}
			else{
				std::cerr << "WARNING: beag::ElectronIDProducer::passed(): PAT electron ID not available: " << id_name << std::endl;
				return false;
			}
		}else if(id_name.find("_no_iso") != std::string::npos){

			size_t start_pos = id_name.rfind("_no_iso");
			if(start_pos != std::string::npos)
				id_name.replace(start_pos,7,"");
		
			if(electron->isElectronIDAvailable(id_name)){
				unsigned long long bit_pattern = electron->electronID(id_name);
				std::vector<unsigned long long> bits_to_check;
				bits_to_check.push_back(0);	// eID
				//bits_to_check.push_back(1);	// iso
				bits_to_check.push_back(2);	// conv. rej.
				bits_to_check.push_back(3);	// ip
				passes_id = true;
				for(std::vector<unsigned long long>::iterator bit = bits_to_check.begin();
					bit != bits_to_check.end();
					++bit){
					if(!(bit_pattern & ((unsigned long long) 1 << (unsigned long long) *bit )))
						passes_id = false;
				}

				if(verbose){
					std::cout << "ID name: " << id_name << " passed: " << passes_id << " output: " << electron->electronID(id_name) << std::endl;
					std::cout << "printing id: " << std::endl;
					for(int i = 0; i < 64; ++i){
						if(((unsigned long long) electron->electronID(id_name) & ((unsigned long long) 1 << (unsigned long long) i) ))
							std::cout << "1";
						else
							std::cout << "0";
					}
					std::cout << std::endl;
					std::cout << " --- " << std::endl;
				}
			}
			else{
				std::cerr << "WARNING: beag::ElectronIDProducer::passed(): PAT electron ID not available: " << id_name << std::endl;
				return false;
			}
		}else{
			/*
			 * bit pattern in CiC IDs:
			 * 1000 - eID cuts passed
			 * 0100 - iso cuts passed
			 * 0010 - conversion rejection
			 * 0001 - ip cut 
			 * and combinations thereof,
			 * cf. https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCategoryBasedElectronID
			 */
			if(electron->isElectronIDAvailable(id_name)){
				unsigned long long bit_pattern = electron->electronID(id_name);
				std::vector<unsigned long long> bits_to_check;
				bits_to_check.push_back(0);	// eID
				bits_to_check.push_back(1);	// iso
				bits_to_check.push_back(2);	// conv. rej.
				bits_to_check.push_back(3);	// ip
				passes_id = true;
				for(std::vector<unsigned long long>::iterator bit = bits_to_check.begin();
					bit != bits_to_check.end();
					++bit){
					if(!(bit_pattern & ((unsigned long long) 1 << (unsigned long long) *bit )))
						passes_id = false;
				}

				if(verbose){
					std::cout << "ID name: " << id_name << " passed: " << passes_id << " output: " << electron->electronID(id_name) << std::endl;
					std::cout << "printing id: " << std::endl;
					for(int i = 0; i < 64; ++i){
						if(((unsigned long long) electron->electronID(id_name) & ((unsigned long long) 1 << (unsigned long long) i) ))
							std::cout << "1";
						else
							std::cout << "0";
					}
					std::cout << std::endl;
					std::cout << " --- " << std::endl;
				}
			}
			else{
				std::cerr << "WARNING: beag::ElectronIDProducer::passed(): PAT electron ID not available: " << id_name << std::endl;
				return false;
			}
		}

		return passes_id;
	}

	if(manual_ids.find(id_name) != manual_ids.end()){
		return calculate_id(electron, id_name);
	}

	return false;
}

bool beag::ElectronIDProducer::calculate_id(const pat::Electron *electron, std::string id_name)
{
	double barrel_sigma_ieta_ieta = 999;
	double barrel_delta_phi = 999;
	double barrel_delta_eta = 999;
	double barrel_HoE = 999;
	double endcap_sigma_ieta_ieta = 999;
	double endcap_delta_phi = 999;
	double endcap_delta_eta = 999;
	double endcap_HoE = 999;

	if(id_name == "simpleEleId70Run2010"){
		barrel_sigma_ieta_ieta = 0.01;
		barrel_delta_phi = 0.03;
		barrel_delta_eta = 0.004;
		barrel_HoE = 0.025;
		endcap_sigma_ieta_ieta = 0.03;
		endcap_delta_phi = 0.02;
		endcap_delta_eta = 0.005;
		endcap_HoE = 0.025;
	}else if(id_name == "simpleEleId95Run2010"){
		barrel_sigma_ieta_ieta = 0.01;
		barrel_delta_phi = 0.8;
		barrel_delta_eta = 0.007;
		barrel_HoE = 0.15;
		endcap_sigma_ieta_ieta = 0.03;
		endcap_delta_phi = 0.7;
		endcap_delta_eta = 0.01;
		endcap_HoE = 0.07;
	}else if(id_name == "simpleEleId70Run2011"){
		barrel_sigma_ieta_ieta = 0.01;
		barrel_delta_phi = 0.02;
		barrel_delta_eta = 0.004;
		endcap_sigma_ieta_ieta = 0.031;
		endcap_delta_phi = 0.021;
		endcap_delta_eta = 0.005;
	}else if(id_name == "simpleEleId95Run2011"){
		barrel_sigma_ieta_ieta = 0.012;
		barrel_delta_phi = 0.8;
		barrel_delta_eta = 0.007;
		endcap_sigma_ieta_ieta = 0.031;
		endcap_delta_phi = 0.7;
		endcap_delta_eta = 0.011;
	}else{
		std::cerr << "WARNING: beag::ElectronIDProducer::calculate_id(): cant calculate ID: " << id_name << std::endl;
		return false;
	}

	bool pass = false;

	if(electron->isEB()){	// barrel
		if(
			electron->sigmaIetaIeta() < barrel_sigma_ieta_ieta &&
			fabs(electron->deltaPhiSuperClusterTrackAtVtx()) < barrel_delta_phi &&
			fabs(electron->deltaEtaSuperClusterTrackAtVtx()) < barrel_delta_eta &&
			electron->hadronicOverEm() < barrel_HoE
		)
			pass = true;
		else
			pass = false;
	}else{	// endcap
		if(
			electron->sigmaIetaIeta() < endcap_sigma_ieta_ieta &&
			fabs(electron->deltaPhiSuperClusterTrackAtVtx()) < endcap_delta_phi &&
			fabs(electron->deltaEtaSuperClusterTrackAtVtx()) < endcap_delta_eta &&
			electron->hadronicOverEm() < endcap_HoE
		)
			pass = true;
		else
			pass = false;
	}

	return pass;
}
