#include "../interface/JetCollection.h"

namespace beag{
template <class CMSSWObject>
beag::JetCollection<CMSSWObject>::JetCollection(TTree *tree, TFile *outfile, std::string ident) : beag::Collection<CMSSWObject, beag::Jet>(tree, outfile, ident)
{
	this->tree->Bronch(ident.c_str(),get_type_string(),&this->beag_objects);
	btag_associator = new beag::BTagAssociator<CMSSWObject>;
	jet_corrector = NULL;
	is_mc = false;
}

template <class CMSSWObject>
beag::JetCollection<CMSSWObject>::~JetCollection()
{
	if(btag_associator){
		delete btag_associator;
		btag_associator = NULL;
	}
}

template <class CMSSWObject>
void beag::JetCollection<CMSSWObject>::fill_jet_common(typename edm::View<CMSSWObject>::const_iterator cmssw_jet, beag::Jet &beag_jet)
{
	beag_jet.jet_area = cmssw_jet->jetArea();
}

template <>
void beag::JetCollection<pat::Jet>::fill_special_vars(edm::View<pat::Jet>::const_iterator cmssw_jet, beag::Jet &beag_jet)
{
	fill_jet_common(cmssw_jet, beag_jet);

	if(cmssw_jet->isCaloJet() || cmssw_jet->isJPTJet()){
		beag_jet.emf = cmssw_jet->emEnergyFraction();
		beag_jet.n90Hits = cmssw_jet->jetID().n90Hits;
		beag_jet.fHPD = cmssw_jet->jetID().fHPD;
	}else{
		beag_jet.nconstituents = cmssw_jet->numberOfDaughters();	// number of constituents
		beag_jet.chf = cmssw_jet->chargedHadronEnergyFraction();		// charged hardron energy fraction
		beag_jet.nhf = cmssw_jet->neutralHadronEnergyFraction();		// neutral hardon energy fraction
		beag_jet.nemf = cmssw_jet->neutralEmEnergyFraction();		// neutral em energy fraction
		beag_jet.cemf = cmssw_jet->chargedEmEnergyFraction();		// charged em energy fraction
		beag_jet.cmulti = cmssw_jet->chargedMultiplicity();		// charged multiplicity
	}

/*	// print available pat jec
	std::vector<std::string> jeclevels = cmssw_jet->availableJECLevels();
	for(std::vector<std::string>::iterator l = jeclevels.begin();
		l != jeclevels.end();
		++l){
		std::cout << "level: " << *l << std::endl;
	}
	std::cout << "---" << std::endl;
*/

	// fill btag information from pat jet for selected algorithms
	for(std::vector<std::string>::iterator btag_algo = this->selected_items->begin();
		btag_algo != this->selected_items->end();
		++btag_algo){
		beag_jet.btags.push_back(cmssw_jet->bDiscriminator(*btag_algo));
	}

/*
	std::cout << "area: " << cmssw_jet->jetArea() << std::endl;
	std::cout << "0.5*0.5*pi: " << 0.5*0.5*TMath::Pi() << std::endl;
	std::cout << "corrected jet pt: " << cmssw_jet->polarP4().pt() << std::endl;
	std::cout << "uncorrected jet pt PAT: " <<  cmssw_jet->correctedJet("Uncorrected").polarP4().pt() << std::endl;
	std::cout << "beag pt: " << beag_jet.pt << std::endl;
	std::cout << "beag area: " << beag_jet.jet_area << std::endl;
	std::cout << "---" << std::endl;
*/
}

template <class CMSSWObject>
void beag::JetCollection<CMSSWObject>::fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle, const edm::Event& iEvent)
{
//	beag::Collection<CMSSWObject, beag::Jet>::fill_collection(handle);
	//btag_associator->set_handles(handle,*this->selected_items, iEvent);
	fill_corrected_jets(handle);
}

template <>
void beag::JetCollection<reco::CaloJet>::fill_collection(edm::Handle<edm::View<reco::CaloJet> > handle, const edm::Event& iEvent)
{
	btag_associator->set_handles(handle,*this->selected_items, iEvent);
	fill_corrected_jets(handle);
}

template <>
void beag::JetCollection<reco::PFJet>::fill_collection(edm::Handle<edm::View<reco::PFJet> > handle, const edm::Event& iEvent)
{
	btag_associator->set_handles(handle,*this->selected_items, iEvent);
	fill_corrected_jets(handle);
}

template <class CMSSWObject>
void beag::JetCollection<CMSSWObject>::fill_common_vars(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, beag::Jet &beag_obj)
{
	beag::Collection<CMSSWObject, beag::Jet>::fill_common_vars(cmssw_obj, beag_obj);
}
// If JEC are applied at PAT level should take this function 
// to ensure uncorrected jets are taken
// If JEC not applied at PAT level may get
// error JEC set does not exist.
// jecSetsAvailable should stop this from happening.
template <>
void beag::JetCollection<pat::Jet>::fill_common_vars(edm::View<pat::Jet>::const_iterator cmssw_obj, beag::Jet &beag_obj)
{
	if(cmssw_obj->jecSetsAvailable()){
		beag_obj.eta = cmssw_obj->correctedJet("Uncorrected").polarP4().eta();
		beag_obj.phi = cmssw_obj->correctedJet("Uncorrected").polarP4().phi();
		beag_obj.mass = cmssw_obj->correctedJet("Uncorrected").polarP4().mass();
		beag_obj.pt = cmssw_obj->correctedJet("Uncorrected").polarP4().pt();
	}else{
		beag_obj.eta = cmssw_obj->polarP4().eta();
		beag_obj.phi = cmssw_obj->polarP4().phi();
		beag_obj.mass = cmssw_obj->polarP4().mass();
		beag_obj.pt = cmssw_obj->polarP4().pt();
	}

}

template <class CMSSWObject>
void beag::JetCollection<CMSSWObject>::fill_corrected_jets(typename edm::Handle<edm::View<CMSSWObject> > handle)
{
	this->outfile->cd();
	this->beag_objects->clear();

	this->cmssw_handle = handle;
	for(typename edm::View<CMSSWObject>::const_iterator cmssw_obj = handle->begin();
		cmssw_obj != handle->end();
		++cmssw_obj){
		beag::Jet *beag_obj = new beag::Jet();

		fill_common_vars(cmssw_obj, *beag_obj);
		fill_special_vars(cmssw_obj, *beag_obj);
		if(jet_corrector) correct_jet(cmssw_obj, *beag_obj);
		// don't do matching for very low pt jets (uncorrected pt!) to
		// reduce time for matching
		//if(is_mc && cmssw_obj->pt() > 8.) match_to_genjet(cmssw_obj, *beag_obj);
		if(is_mc){
			match_to_genjet(cmssw_obj, *beag_obj);
			write_gen_info(cmssw_obj, *beag_obj);
		}

		this->pt_sorter->add(beag_obj);
	}

	std::vector<beag::Jet*> tmp_objects = this->pt_sorter->get_sorted();

	for(std::vector<beag::Jet*>::iterator tmp_obj = tmp_objects.begin();
		tmp_obj != tmp_objects.end();
		++tmp_obj){
		this->beag_objects->push_back(**tmp_obj);
		delete *tmp_obj;
	}

	this->pt_sorter->clear();
}

template <class CMSSWObject>
void beag::JetCollection<CMSSWObject>::match_to_genjet(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, beag::Jet &beag_obj)
{
	double max_dR_diff = 0.5;
	double min_dR = -1;
	double match_pt = -1;
	double match_eta = -1;
	double match_phi = -1;
	double match_mass = -1;
	for(edm::View<reco::GenJet>::const_iterator gen_jet = gen_jetHandle->begin();
		gen_jet != gen_jetHandle->end();
		++gen_jet){
		double current_dR = ROOT::Math::VectorUtil::DeltaR(gen_jet->p4(),cmssw_obj->p4());
		if(current_dR < max_dR_diff && (current_dR < min_dR || min_dR == -1)){
			min_dR = current_dR;
			match_pt = gen_jet->pt();
			match_eta = gen_jet->eta();
			match_phi = gen_jet->phi();
			match_mass = gen_jet->mass();
		}
	}
	beag_obj.pt_genjet = match_pt;
	beag_obj.eta_genjet = match_eta;
	beag_obj.phi_genjet = match_phi;
	beag_obj.mass_genjet = match_mass;
}

template <>
void beag::JetCollection<pat::Jet>::match_to_genjet(edm::View<pat::Jet>::const_iterator cmssw_obj, beag::Jet &beag_obj)
{
	if(cmssw_obj->genJet() != NULL){
		beag_obj.pt_genjet = cmssw_obj->genJet()->pt();
		beag_obj.eta_genjet = cmssw_obj->genJet()->eta();
		beag_obj.phi_genjet = cmssw_obj->genJet()->phi();
		beag_obj.mass_genjet = cmssw_obj->genJet()->mass();
	}else{
		beag_obj.pt_genjet = -1;
		beag_obj.eta_genjet = -1;
		beag_obj.phi_genjet = -1;
		beag_obj.mass_genjet = -1;
	}
}

template <class CMSSWObject>
void beag::JetCollection<CMSSWObject>::correct_jet(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, beag::Jet &beag_obj)
{
	double jec = 1;
	if(jet_corrector){
		jec = jet_corrector->correction(cmssw_obj->p4());
	}

	beag_obj.eta = (jec*cmssw_obj->polarP4()).eta();
	beag_obj.phi = (jec*cmssw_obj->polarP4()).phi();
	beag_obj.mass = (jec*cmssw_obj->polarP4()).mass();
	beag_obj.pt = (jec*cmssw_obj->polarP4()).pt();
}

template <>
void beag::JetCollection<reco::PFJet>::write_gen_info(edm::View<reco::PFJet>::const_iterator cmssw_obj, beag::Jet &beag_obj)
{
}

template <>
void beag::JetCollection<reco::JPTJet>::write_gen_info(edm::View<reco::JPTJet>::const_iterator cmssw_obj, beag::Jet &beag_obj)
{
}

template <>
void beag::JetCollection<reco::CaloJet>::write_gen_info(edm::View<reco::CaloJet>::const_iterator cmssw_obj, beag::Jet &beag_obj)
{
}

template <>
void beag::JetCollection<pat::Jet>::write_gen_info(edm::View<pat::Jet>::const_iterator cmssw_jet, beag::Jet &beag_jet)
{
	// write information of initiating parton
	if(cmssw_jet->genParton() != 0){
		beag_jet.mc_eta = cmssw_jet->genParton()->polarP4().eta();
		beag_jet.mc_phi = cmssw_jet->genParton()->polarP4().phi();
		beag_jet.mc_mass = cmssw_jet->genParton()->polarP4().mass();
		beag_jet.mc_pt = cmssw_jet->genParton()->polarP4().pt();
		beag_jet.mc_id = cmssw_jet->genParton()->pdgId();

		beag_jet.mc_matched = true;
	}else{
		beag_jet.mc_id = cmssw_jet->partonFlavour();
		beag_jet.mc_matched = false;
	}
}


template <class CMSSWObject>
void beag::JetCollection<CMSSWObject>::set_jetCorrector(std::string mJetCorServiceName, const edm::EventSetup& iSetup)
{
	if(mJetCorServiceName != ""){
		jet_corrector = const_cast<JetCorrector*>(JetCorrector::getJetCorrector(mJetCorServiceName,iSetup));
	}
}

template <class CMSSWObject>
void beag::JetCollection<CMSSWObject>::write_gen_info(typename edm::View<CMSSWObject>::const_iterator cmssw_jet, beag::Jet &beag_jet)
{

	// write information of initiating parton
	if(cmssw_jet->genParton() != 0){
		beag_jet.mc_eta = cmssw_jet->genParton()->polarP4().eta();
		beag_jet.mc_phi = cmssw_jet->genParton()->polarP4().phi();
		beag_jet.mc_mass = cmssw_jet->genParton()->polarP4().mass();
		beag_jet.mc_pt = cmssw_jet->genParton()->polarP4().pt();
		beag_jet.mc_id = cmssw_jet->genParton()->pdgId();

		beag_jet.mc_matched = true;
	}else{
		beag_jet.mc_matched = false;
	}
}

template <>
void beag::JetCollection<reco::JPTJet>::fill_special_vars(edm::View<reco::JPTJet>::const_iterator cmssw_jet, beag::Jet &beag_jet)
{
	fill_jet_common(cmssw_jet, beag_jet);
	fill_jet_id(cmssw_jet, beag_jet);
	beag_jet.btags = btag_associator->fill_bdiscriminators(cmssw_jet);//add when set_handles for JPT added
	// FIXME: fill em energy fraction
	std::cerr << "WARNING: JPTJet no btag information implemented" << std::endl;
	std::cerr << "WARNING: JPTJet EMEnergyFraction not yet implemented, aborting..." << std::endl;
	exit(1);
}

template <>
void beag::JetCollection<reco::CaloJet>::fill_special_vars(edm::View<reco::CaloJet>::const_iterator cmssw_jet, beag::Jet &beag_jet)
{
	fill_jet_common(cmssw_jet, beag_jet);

	fill_jet_id(cmssw_jet, beag_jet);
	beag_jet.emf = cmssw_jet->emEnergyFraction();
	beag_jet.btags = btag_associator->fill_bdiscriminators(cmssw_jet);
}

template <>
void beag::JetCollection<reco::PFJet>::fill_special_vars(edm::View<reco::PFJet>::const_iterator cmssw_jet, beag::Jet &beag_jet)
{
	fill_jet_common(cmssw_jet, beag_jet);

	beag_jet.nconstituents = cmssw_jet->numberOfDaughters();	// number of constituents
	beag_jet.chf = cmssw_jet->chargedHadronEnergyFraction();		// charged hardron energy fraction
	beag_jet.nhf = cmssw_jet->neutralHadronEnergyFraction();		// neutral hardon energy fraction
	beag_jet.nemf = cmssw_jet->neutralEmEnergyFraction();		// neutral em energy fraction
	beag_jet.cemf = cmssw_jet->chargedEmEnergyFraction();		// charged em energy fraction
	beag_jet.cmulti = cmssw_jet->chargedMultiplicity();		// charged multiplicity

	beag_jet.btags = btag_associator->fill_bdiscriminators(cmssw_jet);
}

template <class CMSSWObject>
void beag::JetCollection<CMSSWObject>::fill_jet_id(typename edm::View<CMSSWObject>::const_iterator cmssw_jet, beag::Jet &beag_jet)
{
	unsigned int idx;

	//Get jetID structure for this jet
	idx = cmssw_jet - this->cmssw_handle->begin();
	edm::RefToBase<CMSSWObject> jetRef = this->cmssw_handle->refAt(idx);
	reco::JetID jetId = (*jetIDHandle)[jetRef];

	beag_jet.fHPD = jetId.fHPD;
	beag_jet.n90Hits = jetId.n90Hits;
}

template <class CMSSWObject>
const char* beag::JetCollection<CMSSWObject>::get_type_string()
{
	return "std::vector<beag::Jet>";
}

template class beag::JetCollection<reco::CaloJet>;
template class beag::JetCollection<reco::PFJet>;
template class beag::JetCollection<reco::JPTJet>;
template class beag::JetCollection<pat::Jet>;
}	// end namespace beag
