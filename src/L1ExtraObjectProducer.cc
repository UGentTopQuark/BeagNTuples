#include "../interface/L1ExtraObjectProducer.h"

beag::L1ExtraObjectProducer::L1ExtraObjectProducer()
{
}

beag::L1ExtraObjectProducer::~L1ExtraObjectProducer()
{
}

void beag::L1ExtraObjectProducer::set_trigger_names(std::vector<std::string> *trigger_names)
{
	// delete old trigger cuts
	for(std::map<std::string, L1ExtraCuts*>::iterator cut = trigger_cuts.begin();
		cut != trigger_cuts.end();
		++cut){
		delete cut->second;
	}
	trigger_cuts.clear();

	for(std::vector<std::string>::iterator trigger_name = trigger_names->begin();
		trigger_name != trigger_names->end();	
		++trigger_name){
		if(verbose) std::cout << "L1ExtraObjectProducer: adding trigger_name: " << *trigger_name << std::endl;
		L1ExtraCuts *cuts = get_l1_extra_cuts(*trigger_name);
		trigger_cuts[*trigger_name] = cuts;
	}
	this->trigger_names = trigger_names;
}

void beag::L1ExtraObjectProducer::set_trigger_objects(std::vector<beag::TriggerObject> *trigger_objects)
{
	this->trigger_objects = trigger_objects;
}

beag::L1ExtraCuts* beag::L1ExtraObjectProducer::get_l1_extra_cuts(std::string trigger_name)
{
	L1ExtraCuts *cuts = new L1ExtraCuts();

	if(trigger_name == "hltL1sL1SingleMu7"){
		cuts->set_min_pt(7.);
		cuts->set_min_quality(3.);
	}else{
		cuts->set_min_pt(99999.);
		cuts->set_min_quality(99999.);
		//std::cerr << "WARNING: L1ExtraObjectProdcuer::get_l1_extra_cuts(): can not find valid L1Extra selection cuts for: " << trigger_name << std::endl;
	}

	return cuts;
}

void beag::L1ExtraObjectProducer::fill_l1_objects(edm::Handle<l1extra::L1MuonParticleCollection> &l1muons)
{
	/*
	// FIXME: trigger objects appear twice in collection...
	if(verbose) std::cout << "trigger obj size before adding l1 objects: " << trigger_objects->size() << std::endl;
	for(l1extra::L1MuonParticleCollection::const_iterator l1_muon=l1muons->begin(); l1_muon!=l1muons->end(); l1_muon++){
		beag::TriggerObject obj;
		unsigned long long nbeag_trig = 0;
		bool obj_filled = false;
		if(verbose) std::cout << "--- next l1 muon ---" << std::endl;
		for(std::vector<std::string>::iterator trigger_name = trigger_names->begin();
			trigger_name != trigger_names->end();
			++trigger_name){
			L1ExtraCuts *l1_cuts = trigger_cuts[*trigger_name];
			if(pass(&(*l1_muon), l1_cuts)){
				if(!obj_filled){
					obj.eta = l1_muon->polarP4().eta();
					obj.phi = l1_muon->polarP4().phi();
					obj.pt = l1_muon->polarP4().pt();
					obj.mass = l1_muon->polarP4().mass();
					obj.bx = l1_muon->bx();
					obj.quality = l1_muon->gmtMuonCand().quality();
					obj.charge = l1_muon->charge();
					obj.is_forward = l1_muon->isForward();
					obj.detector = l1_muon->gmtMuonCand().detector();
					
					if(verbose){
						std::cout << "L1 muon (pt,eta,phi)" << std::endl;
						std::cout << "L1 muon (" << obj.pt << "," << obj.eta << "," << obj.phi << ")" << std::endl;
						std::cout << "L1 muon (is forward: " << obj.is_forward << ")" << std::endl;
						std::cout << "L1 muon (quality: " << obj.quality << ")" << std::endl;
						std::cout << "L1 muon (detector: " << obj.detector << ")" << std::endl;
						std::cout << "L1 muon (bx: " << obj.bx << ")" << std::endl;
					}

					obj_filled = true;
				}
				obj.l1triggered[nbeag_trig] = true;
			}
			++nbeag_trig;
		}
		if(obj_filled)
			trigger_objects->push_back(obj);
	}
	*/
	if(verbose) std::cout << "=== end fill l1 objects ===" << std::endl;
	if(verbose) std::cout << "trigger obj size after adding l1 objects: " << trigger_objects->size() << std::endl;
}

bool beag::L1ExtraObjectProducer::pass(const l1extra::L1MuonParticle *l1_muon, L1ExtraCuts* cuts)
{
	if(l1_muon->pt() >= cuts->get_min_pt() &&
		l1_muon->gmtMuonCand().quality() >= cuts->get_min_quality())
		return true;
	else
		return false;
}
