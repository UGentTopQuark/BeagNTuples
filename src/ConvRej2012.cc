#include "../interface/ConvRej2012.h"

void beag::ConvRej2012::set_handles(edm::Handle<reco::BeamSpot> beamSpotHandle,
					edm::Handle<reco::ConversionCollection> hConversions)
{
	this->beamSpotHandle = beamSpotHandle;
        this->hConversions = hConversions;
}

void beag::ConvRej2012::calculate(const reco::GsfElectron *electron)
{
	if(!electron->gsfTrack().isNull() && beamSpotHandle.isValid()){
        	const reco::BeamSpot &beamspot = *beamSpotHandle.product();
        	passconversionveto = !ConversionTools::hasMatchedConversion(*electron,hConversions,beamspot.position());
	}else{
		passconversionveto = 1;
	}
}
