#include "../interface/PrimaryVertexProducer.h"

beag::PrimaryVertexProducer::PrimaryVertexProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	primary_vertices = new std::vector<beag::PrimaryVertex>();
	primary_vertices->push_back(beag::PrimaryVertex());
	tree->Bronch(ident.c_str(),"std::vector<beag::PrimaryVertex>",&primary_vertices);
}

beag::PrimaryVertexProducer::~PrimaryVertexProducer()
{
	if(primary_vertices){
		delete primary_vertices;
		primary_vertices=NULL;
	}
}

void beag::PrimaryVertexProducer::fill_primary_vertices(edm::Handle<std::vector<reco::Vertex> > vertex_handle)
{
	primary_vertices->clear();

	/*
	 *	for each primary vertex in the reco:: collection create a
	 *	beag::PrimaryVertex and add it to the according vector of the
	 *	new, small collection
	 */
	for(std::vector<reco::Vertex>::const_iterator vertex = vertex_handle->begin();
		vertex != vertex_handle->end();
		++vertex){
		beag::PrimaryVertex beag_vertex;
		beag_vertex.rho = vertex->position().Rho();
		beag_vertex.z = vertex->z();	
		beag_vertex.ndof = vertex->ndof();	
		beag_vertex.is_fake = vertex->isFake();	
		primary_vertices->push_back(beag_vertex);
	}
}
