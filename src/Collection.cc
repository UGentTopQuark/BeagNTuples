#include "../interface/Collection.h"

namespace beag{

template <class CMSSWObject, class BeagObject>
beag::Collection<CMSSWObject, BeagObject>::Collection(TTree *tree, TFile *outfile, std::string ident)
{
	mc_matching = true;
	this->tree = tree;
	this->outfile = outfile;

	pt_sorter = new PtSorter<BeagObject>();

	beag_objects = new std::vector<BeagObject>();
}

template <class CMSSWObject, class BeagObject>
beag::Collection<CMSSWObject, BeagObject>::~Collection()
{
	if(beag_objects){
		delete beag_objects;
		beag_objects = NULL;
	}
	if(pt_sorter){
		delete pt_sorter;
		pt_sorter = NULL;
	}
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle)
{
	outfile->cd();
	beag_objects->clear();

	this->cmssw_handle = handle;

	for(typename edm::View<CMSSWObject>::const_iterator cmssw_obj = handle->begin();
		cmssw_obj != handle->end();
		++cmssw_obj){
		BeagObject *beag_obj = new BeagObject();

		fill_common_vars(cmssw_obj, *beag_obj);
		fill_special_vars(cmssw_obj, *beag_obj);

		pt_sorter->add(beag_obj);
	}

	std::vector<BeagObject*> tmp_objects = pt_sorter->get_sorted();

	for(typename std::vector<BeagObject*>::iterator tmp_obj = tmp_objects.begin();
		tmp_obj != tmp_objects.end();
		++tmp_obj){
		beag_objects->push_back(**tmp_obj);
		delete *tmp_obj;
	}

	pt_sorter->clear();
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::fill_common_vars(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
	beag_obj.eta = cmssw_obj->polarP4().eta();
	beag_obj.phi = cmssw_obj->polarP4().phi();
	beag_obj.mass = cmssw_obj->polarP4().mass();
	beag_obj.pt = cmssw_obj->polarP4().pt();
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::fill_special_vars(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_selection_list(std::vector<std::string> *selection_list)
{
	selected_items = selection_list;
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_tag_list(std::vector<std::string> *tag_list)
{
	selected_tags = tag_list;
}

template <class CMSSWObject, class BeagObject>
std::vector<BeagObject>* beag::Collection<CMSSWObject, BeagObject>::get_beag_objects()
{
	return beag_objects;
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_mc_matching(bool match_mc)
{
	mc_matching = match_mc;
}

template class beag::Collection<reco::Muon, beag::Muon>;
template class beag::Collection<pat::Muon, beag::Muon>;

template class beag::Collection<reco::GsfElectron, beag::Electron>;
template class beag::Collection<pat::Electron, beag::Electron>;

template class beag::Collection<reco::CaloJet, beag::Jet>;
template class beag::Collection<reco::PFJet, beag::Jet>;
template class beag::Collection<reco::JPTJet, beag::Jet>;
template class beag::Collection<pat::Jet, beag::Jet>;

template class beag::Collection<pat::MET, beag::MET>;
template class beag::Collection<reco::CaloMET, beag::MET>;
template class beag::Collection<reco::PFMET, beag::MET>;
template class beag::Collection<reco::MET, beag::MET>;

}	// end namespace beag
