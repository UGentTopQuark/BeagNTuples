//#include "analyzers/EventSelection/interface/PtSorter.h"

template <class MyObject>
void PtSorter<MyObject>::add(MyObject *obj)
{
	objects.push_back(obj);
}

template <class MyObject>
bool PtSorter<MyObject>::compare_pt(const MyObject *obj1,
		          const MyObject *obj2)
{
	return obj1->pt > obj2->pt;
}

template <class MyObject>
std::vector<MyObject*> PtSorter<MyObject>::get_sorted()
{
	std::sort(objects.begin(), objects.end(), PtSorter::compare_pt);
	return objects;
}

template <class MyObject>
void PtSorter<MyObject>::clear()
{
	objects.clear();
}
