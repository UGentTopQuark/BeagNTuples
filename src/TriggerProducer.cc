#include "../interface/TriggerProducer.h"

beag::TriggerProducer::TriggerProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	trigger = new std::vector<beag::Trigger>();
	trigger->push_back(beag::Trigger());
	tree->Bronch(ident.c_str(),"std::vector<beag::Trigger>",&trigger);
}

beag::TriggerProducer::~TriggerProducer()
{
	if(trigger){
		delete trigger;
		trigger = NULL;
	}
}

void beag::TriggerProducer::set_hlt_config_provider(HLTConfigProvider *hltConfig)
{
	this->hltConfig = hltConfig;
}

void beag::TriggerProducer::set_triggers_to_write(std::vector<std::string> *triggers)
{
	this->triggers_to_write = triggers;
}

void beag::TriggerProducer::fill_trigger_information(edm::Handle<edm::TriggerResults> HLTR)
{
	outfile->cd();
	if(!HLTR.isValid()){
		std::cerr << "WARNING: beag::TriggerProducer::fill_trigger_information() no valid trigger object found" << std::endl;
		return;
	}

	trigger->begin()->triggered.clear();

	int nbeag_trig=0;
	for(std::vector<std::string>::iterator trigger_str = triggers_to_write->begin();
		trigger_str != triggers_to_write->end();
		++trigger_str){
		unsigned int trigger_id( hltConfig->triggerIndex(*trigger_str) );
		if(verbose) std::cout << "next trigger id: " << trigger_id << std::endl;
		if(verbose){
			if(HLTR->accept(trigger_id))
				std::cout << "accepted by: " << *trigger_str << " id: " << trigger_id << " : " << HLTR->accept(trigger_id) << std::endl; 
			else
				std::cout << "rejected by: " << *trigger_str << " id: " << trigger_id << " : " << HLTR->accept(trigger_id) << std::endl; 
		}
		if(HLTR->accept(trigger_id)){
			trigger->begin()->triggered.push_back(1);
		}else{
			trigger->begin()->triggered.push_back(0);
		}
		
		++nbeag_trig;
	}
}
