/********************************************************************
* MultiDict.h
* CAUTION: DON'T CHANGE THIS FILE. THIS FILE IS AUTOMATICALLY GENERATED
*          FROM HEADER FILES LISTED IN G__setup_cpp_environmentXXX().
*          CHANGE THOSE HEADER FILES AND REGENERATE THIS FILE.
********************************************************************/
#ifdef __CINT__
#error MultiDict.h/C is only for compilation. Abort cint.
#endif
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define G__ANSIHEADER
#define G__DICTIONARY
#define G__PRIVATE_GVALUE
#include "G__ci.h"
#include "FastAllocString.h"
extern "C" {
extern void G__cpp_setup_tagtableMultiDict();
extern void G__cpp_setup_inheritanceMultiDict();
extern void G__cpp_setup_typetableMultiDict();
extern void G__cpp_setup_memvarMultiDict();
extern void G__cpp_setup_globalMultiDict();
extern void G__cpp_setup_memfuncMultiDict();
extern void G__cpp_setup_funcMultiDict();
extern void G__set_cpp_environmentMultiDict();
}


#include "TObject.h"
#include "TMemberInspector.h"
#include "../interface/BeagObjects/Electron.h"
#include "../interface/BeagObjects/Muon.h"
#include "../interface/BeagObjects/Jet.h"
#include "../interface/BeagObjects/Lepton.h"
#include "../interface/BeagObjects/Particle.h"
#include "../interface/BeagObjects/MET.h"
#include "../interface/BeagObjects/Trigger.h"
#include "../interface/BeagObjects/TriggerObject.h"
#include "../interface/BeagObjects/TTbarGenEvent.h"
#include "../interface/BeagObjects/PrimaryVertex.h"
#include "../interface/BeagObjects/GenParticle.h"
#include "../interface/BeagObjects/EventInformation.h"
#include "../interface/BeagObjects/Conversion.h"
#include <algorithm>
namespace std { }
using namespace std;

#ifndef G__MEMFUNCBODY
#endif

extern G__linked_taginfo G__MultiDictLN_TClass;
extern G__linked_taginfo G__MultiDictLN_TBuffer;
extern G__linked_taginfo G__MultiDictLN_TMemberInspector;
extern G__linked_taginfo G__MultiDictLN_TObject;
extern G__linked_taginfo G__MultiDictLN_vectorlEdoublecOallocatorlEdoublegRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEboolcOallocatorlEboolgRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEROOTcLcLTSchemaHelpercOallocatorlEROOTcLcLTSchemaHelpergRsPgR;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEROOTcLcLTSchemaHelpercOallocatorlEROOTcLcLTSchemaHelpergRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlETVirtualArraymUcOallocatorlETVirtualArraymUgRsPgR;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlETVirtualArraymUcOallocatorlETVirtualArraymUgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_beag;
extern G__linked_taginfo G__MultiDictLN_beagcLcLParticle;
extern G__linked_taginfo G__MultiDictLN_beagcLcLLepton;
extern G__linked_taginfo G__MultiDictLN_beagcLcLConversion;
extern G__linked_taginfo G__MultiDictLN_beagcLcLElectron;
extern G__linked_taginfo G__MultiDictLN_beagcLcLMuon;
extern G__linked_taginfo G__MultiDictLN_beagcLcLJet;
extern G__linked_taginfo G__MultiDictLN_beagcLcLMET;
extern G__linked_taginfo G__MultiDictLN_beagcLcLTrigger;
extern G__linked_taginfo G__MultiDictLN_beagcLcLTriggerObject;
extern G__linked_taginfo G__MultiDictLN_vectorlEvectorlEboolcOallocatorlEboolgRsPgRcOallocatorlEvectorlEboolcOallocatorlEboolgRsPgRsPgRsPgR;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEvectorlEboolcOallocatorlEboolgRsPgRcOallocatorlEvectorlEboolcOallocatorlEboolgRsPgRsPgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_beagcLcLTTbarGenEvent;
extern G__linked_taginfo G__MultiDictLN_beagcLcLPrimaryVertex;
extern G__linked_taginfo G__MultiDictLN_beagcLcLGenParticle;
extern G__linked_taginfo G__MultiDictLN_beagcLcLEventInformation;
extern G__linked_taginfo G__MultiDictLN_vectorlEpairlEintcOdoublegRcOallocatorlEpairlEintcOdoublegRsPgRsPgR;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEpairlEintcOdoublegRcOallocatorlEpairlEintcOdoublegRsPgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEvectorlEdoublecOallocatorlEdoublegRsPgRcOallocatorlEvectorlEdoublecOallocatorlEdoublegRsPgRsPgRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEvectorlEdoublecOallocatorlEdoublegRsPgRcOallocatorlEvectorlEdoublecOallocatorlEdoublegRsPgRsPgRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEvectorlEdoublecOallocatorlEdoublegRsPgRcOallocatorlEvectorlEdoublecOallocatorlEdoublegRsPgRsPgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEstringcOallocatorlEstringgRsPgR;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEstringcOallocatorlEstringgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEvectorlEstringcOallocatorlEstringgRsPgRcOallocatorlEvectorlEstringcOallocatorlEstringgRsPgRsPgRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEvectorlEstringcOallocatorlEstringgRsPgRcOallocatorlEvectorlEstringcOallocatorlEstringgRsPgRsPgRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEvectorlEstringcOallocatorlEstringgRsPgRcOallocatorlEvectorlEstringcOallocatorlEstringgRsPgRsPgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLElectroncOallocatorlEbeagcLcLElectrongRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLElectroncOallocatorlEbeagcLcLElectrongRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLElectroncOallocatorlEbeagcLcLElectrongRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLMuoncOallocatorlEbeagcLcLMuongRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLMuoncOallocatorlEbeagcLcLMuongRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLMuoncOallocatorlEbeagcLcLMuongRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLJetcOallocatorlEbeagcLcLJetgRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLJetcOallocatorlEbeagcLcLJetgRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLJetcOallocatorlEbeagcLcLJetgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLLeptoncOallocatorlEbeagcLcLLeptongRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLLeptoncOallocatorlEbeagcLcLLeptongRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLLeptoncOallocatorlEbeagcLcLLeptongRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLParticlecOallocatorlEbeagcLcLParticlegRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLParticlecOallocatorlEbeagcLcLParticlegRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLParticlecOallocatorlEbeagcLcLParticlegRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLMETcOallocatorlEbeagcLcLMETgRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLMETcOallocatorlEbeagcLcLMETgRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLMETcOallocatorlEbeagcLcLMETgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLTriggercOallocatorlEbeagcLcLTriggergRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLTriggercOallocatorlEbeagcLcLTriggergRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLTriggercOallocatorlEbeagcLcLTriggergRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLTriggerObjectcOallocatorlEbeagcLcLTriggerObjectgRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLTriggerObjectcOallocatorlEbeagcLcLTriggerObjectgRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLTriggerObjectcOallocatorlEbeagcLcLTriggerObjectgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLTTbarGenEventcOallocatorlEbeagcLcLTTbarGenEventgRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLTTbarGenEventcOallocatorlEbeagcLcLTTbarGenEventgRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLTTbarGenEventcOallocatorlEbeagcLcLTTbarGenEventgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLPrimaryVertexcOallocatorlEbeagcLcLPrimaryVertexgRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLPrimaryVertexcOallocatorlEbeagcLcLPrimaryVertexgRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLPrimaryVertexcOallocatorlEbeagcLcLPrimaryVertexgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLGenParticlecOallocatorlEbeagcLcLGenParticlegRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLGenParticlecOallocatorlEbeagcLcLGenParticlegRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLGenParticlecOallocatorlEbeagcLcLGenParticlegRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLEventInformationcOallocatorlEbeagcLcLEventInformationgRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLEventInformationcOallocatorlEbeagcLcLEventInformationgRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLEventInformationcOallocatorlEbeagcLcLEventInformationgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLConversioncOallocatorlEbeagcLcLConversiongRsPgR;
extern G__linked_taginfo G__MultiDictLN_vectorlEbeagcLcLConversioncOallocatorlEbeagcLcLConversiongRsPgRcLcLiterator;
extern G__linked_taginfo G__MultiDictLN_reverse_iteratorlEvectorlEbeagcLcLConversioncOallocatorlEbeagcLcLConversiongRsPgRcLcLiteratorgR;

/* STUB derived class for protected member access */
typedef vector<vector<double,allocator<double> >,allocator<vector<double,allocator<double> > > > G__vectorlEvectorlEdoublecOallocatorlEdoublegRsPgRcOallocatorlEvectorlEdoublecOallocatorlEdoublegRsPgRsPgRsPgR;
typedef vector<vector<string,allocator<string> >,allocator<vector<string,allocator<string> > > > G__vectorlEvectorlEstringcOallocatorlEstringgRsPgRcOallocatorlEvectorlEstringcOallocatorlEstringgRsPgRsPgRsPgR;
typedef vector<beag::Electron,allocator<beag::Electron> > G__vectorlEbeagcLcLElectroncOallocatorlEbeagcLcLElectrongRsPgR;
typedef vector<beag::Muon,allocator<beag::Muon> > G__vectorlEbeagcLcLMuoncOallocatorlEbeagcLcLMuongRsPgR;
typedef vector<beag::Jet,allocator<beag::Jet> > G__vectorlEbeagcLcLJetcOallocatorlEbeagcLcLJetgRsPgR;
typedef vector<beag::Lepton,allocator<beag::Lepton> > G__vectorlEbeagcLcLLeptoncOallocatorlEbeagcLcLLeptongRsPgR;
typedef vector<beag::Particle,allocator<beag::Particle> > G__vectorlEbeagcLcLParticlecOallocatorlEbeagcLcLParticlegRsPgR;
typedef vector<beag::MET,allocator<beag::MET> > G__vectorlEbeagcLcLMETcOallocatorlEbeagcLcLMETgRsPgR;
typedef vector<beag::Trigger,allocator<beag::Trigger> > G__vectorlEbeagcLcLTriggercOallocatorlEbeagcLcLTriggergRsPgR;
typedef vector<beag::TriggerObject,allocator<beag::TriggerObject> > G__vectorlEbeagcLcLTriggerObjectcOallocatorlEbeagcLcLTriggerObjectgRsPgR;
typedef vector<beag::TTbarGenEvent,allocator<beag::TTbarGenEvent> > G__vectorlEbeagcLcLTTbarGenEventcOallocatorlEbeagcLcLTTbarGenEventgRsPgR;
typedef vector<beag::PrimaryVertex,allocator<beag::PrimaryVertex> > G__vectorlEbeagcLcLPrimaryVertexcOallocatorlEbeagcLcLPrimaryVertexgRsPgR;
typedef vector<beag::GenParticle,allocator<beag::GenParticle> > G__vectorlEbeagcLcLGenParticlecOallocatorlEbeagcLcLGenParticlegRsPgR;
typedef vector<beag::EventInformation,allocator<beag::EventInformation> > G__vectorlEbeagcLcLEventInformationcOallocatorlEbeagcLcLEventInformationgRsPgR;
typedef vector<beag::Conversion,allocator<beag::Conversion> > G__vectorlEbeagcLcLConversioncOallocatorlEbeagcLcLConversiongRsPgR;
