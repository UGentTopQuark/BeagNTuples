#include "../interface/TriggerObjectFinder.h"

beag::TriggerObjectFinder::TriggerObjectFinder(TFile *outfile)
{
	this->outfile = outfile;
	trigger_menu = "HLT";
	objects_found = false;
	l1_from_l1extra = false;

	modules_for_trigger = NULL;
}

beag::TriggerObjectFinder::~TriggerObjectFinder()
{
}

void beag::TriggerObjectFinder::find_trigger_objects(edm::Handle<trigger::TriggerEvent> aodTriggerEvent,edm::Handle<edm::TriggerResults> HLTR, HLTConfigProvider *hltConfig)
{
	objects_to_write.clear();
	keys_found.clear();
	objects_found = false;

	this->aodTriggerEvent = aodTriggerEvent;
	this->HLTR = HLTR;

	if(!HLTR.isValid()){
		std::cerr << "WARNING: beag::TriggerObjectFinder::find_trigger_objects() no valid HLTR" << std::endl;
		return;
	}

	for(int nbeag_trig = 0; nbeag_trig < int(triggers_to_write->size());nbeag_trig++){
		std::string trigger_name = (*triggers_to_write)[nbeag_trig];
                //Skip if is one of vetoed triggers
                if(triggers_to_veto.find(trigger_name) != triggers_to_veto.end())
                        continue;
                
		if(verbose) std::cout << " Event passed trigger: " << trigger_name << std::endl;
		if(verbose) std::cout << " Modules for trigger size: " << (*modules_for_trigger)[nbeag_trig].size() << std::endl;
		for(unsigned int nmodule = 0; nmodule < (*modules_for_trigger)[nbeag_trig].size(); ++nmodule){
			get_trigger_objects((*modules_for_trigger)[nbeag_trig][nmodule], (*triggers_to_write)[nbeag_trig], nbeag_trig, nmodule); //hlt trigger objects
		}
	}

	objects_found = true;
}

//Get trigger objects passing a given module. map<keys,trigger_results>
void beag::TriggerObjectFinder::get_trigger_objects(std::string module_name, std::string trigger_name ,int nbeag_trig, int nmodule)
{
	if(module_name == "")
		std::cerr << "ERROR TrigObjProd: No Module Name found for: " <<trigger_name << std::endl;
	
	if(verbose) std::cout << " Trigger: " << trigger_name << ", nbeag_trig " << nbeag_trig << ", module: " << nmodule << ", module name: " << module_name << std::endl; 
	       
	//Now use module name to get keys for trigger objects
	edm::InputTag filterTag = edm::InputTag(module_name, "", trigger_menu);
		
	int filterIndex = aodTriggerEvent->filterIndex(filterTag);


		
	if ( filterIndex < aodTriggerEvent->sizeFilters() ) {
		//These trigger keys point you to the trigger objects passing this filter in the objects collection	
		const trigger::Keys &keys = aodTriggerEvent->filterKeys( filterIndex );
		for ( size_t i = 0; i < keys.size(); i++ ){
		
			if(objects_to_write.find(keys[i]) == objects_to_write.end()){
				objects_to_write[keys[i]].assign(triggers_to_write->size(),std::vector<bool>());
				for(unsigned int ntrigger = 0; ntrigger < triggers_to_write->size(); ++ntrigger){
					objects_to_write[keys[i]][ntrigger].assign((*modules_for_trigger)[ntrigger].size(),false);
				}
			}
			if(verbose) std::cout << " Found keys for: " << module_name << " key: " << keys[i] << " module: " << nmodule << " nbeag: " << nbeag_trig << std::endl;
			objects_to_write[keys[i]][nbeag_trig][nmodule] = true;
			/*
			if(module == 0) keys_found[keys[i]].push_back(nbeag_trig);
			*/
 		}
	}
}

bool beag::TriggerObjectFinder::check_objects_found()
{
	return objects_found;
}

std::map<int,std::vector<std::vector<bool> > > beag::TriggerObjectFinder::get_objects_to_write()
{
	return objects_to_write;
}

std::map<int,std::vector<int> > beag::TriggerObjectFinder::get_keys_found()
{
	return keys_found;
}

void beag::TriggerObjectFinder::set_triggers_to_veto(std::vector<std::string> *veto_triggers)
{
	if(!triggers_to_veto.empty()) triggers_to_veto.clear();
	for(std::vector<std::string>::iterator vtrig = veto_triggers->begin();vtrig != veto_triggers->end();vtrig++)
		triggers_to_veto[*vtrig] = true;
}

void beag::TriggerObjectFinder::set_hlt_config_provider(HLTConfigProvider *hltConfig)
{
	this->hltConfig = hltConfig;
}

void beag::TriggerObjectFinder::set_triggers_to_write(std::vector<std::string> *triggers)
{
	this->triggers_to_write = triggers;
}  

void beag::TriggerObjectFinder::set_trigger_menu(std::string trigger_menu)
{
	this->trigger_menu = trigger_menu;
}

void beag::TriggerObjectFinder::set_l1_from_l1extra(bool from_l1extra)
{
	this->l1_from_l1extra = from_l1extra;
}
