#include "../interface/BTagAssociator.h"


template <class CMSSWObject>
beag::BTagAssociator<CMSSWObject>::BTagAssociator()
{
}

template <class CMSSWObject>
void beag::BTagAssociator<CMSSWObject>::set_handles(typename edm::Handle<edm::View<CMSSWObject> > jetHandle, std::vector<std::string> btag_algos,const edm::Event& iEvent)
{
	this->jetHandle = jetHandle;

	jetAlgorithms.resize(btag_algos.size());
	for(size_t i = 0; i < btag_algos.size(); i++){
		edm::Handle<reco::JetTagCollection> bTagHandle;
		iEvent.getByLabel(btag_algos[i], bTagHandle);
		jetAlgorithms[i] = bTagHandle;
	}
	
	
}

template <class CMSSWObject>
std::vector<double> beag::BTagAssociator<CMSSWObject>::fill_bdiscriminators(typename edm::View<CMSSWObject>::const_iterator cmssw_obj)
{

	std::vector<double> btag_values;

	for (size_t i=0; i < jetAlgorithms.size(); i++) {
		const reco::JetTagCollection & bTags = *(jetAlgorithms[i].product());

		//loop over all jets with btags for this algoritm and find the right one
		for(size_t j = 0; j < bTags.size(); j++){	
			if(cmssw_obj->p4() == bTags[j].first->p4()){ 
				btag_values.push_back(bTags[j].second);
				break;
			}
		}
	}

	return btag_values;
}

template class beag::BTagAssociator<reco::CaloJet>;
template class beag::BTagAssociator<reco::PFJet>;
template class beag::BTagAssociator<reco::JPTJet>;
template class beag::BTagAssociator<pat::Jet>;
