#include "../interface/PartnerTrackFinder.h"

void beag::PartnerTrackFinder::set_handles(edm::Handle<reco::TrackCollection> tracks_h,
					edm::Handle<DcsStatusCollection> dcsHandle)
{
	this->tracks_h = tracks_h;
	this->dcsHandle = dcsHandle;

	evt_bField = 0.;
}

void beag::PartnerTrackFinder::next_event(const edm::EventSetup& iSetup, bool is_data)
{
	using namespace edm;
	// need the magnetic field
	//
	// if isData then derive bfield using the
	// magnet current from DcsStatus
	// otherwise take it from the IdealMagneticFieldRecord
	if(is_data){
	     // scale factor = 3.801/18166.0 which are
	     // average values taken over a stable two
	     // week period
	     float currentToBFieldScaleFactor = 2.09237036221512717e-04;
	     float current = 0;
	     if(dcsHandle->size() > 0) current = (*dcsHandle)[0].magnetCurrent();
	     evt_bField = current*currentToBFieldScaleFactor;
	}
	else{
	    edm::ESHandle<MagneticField> magneticField;
	    iSetup.get<IdealMagneticFieldRecord>().get(magneticField);
	        
	    evt_bField = magneticField->inTesla(GlobalPoint(0.,0.,0.)).z();
	}
}

void beag::PartnerTrackFinder::calculate(const reco::GsfElectron *electron)
{
	ConversionFinder convFinder;
	ConversionInfo convInfo = convFinder.getConversionInfo(*electron, tracks_h, evt_bField);
	
	dist = convInfo.dist();
	dcot = convInfo.dcot();
	convradius = convInfo.radiusOfConversion();
	convPoint = convInfo.pointOfConversion();
}
