#include "../interface/PDFWeightProducer.h"

beag::PDFWeightProducer::PDFWeightProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	pdf_weights = new std::vector<std::vector<double> >();
	tree->Bronch(ident.c_str(),"std::vector<std::vector<double> >",&pdf_weights);
}

beag::PDFWeightProducer::~PDFWeightProducer()
{
	if(pdf_weights){
		delete pdf_weights;
		pdf_weights = NULL;
	}
}

void beag::PDFWeightProducer::fill_pdf_weights(edm::Handle<std::vector<double> > weightHandle, unsigned int pdf_index)
{
	if(pdf_weights->size()-1 < pdf_index || pdf_weights->size() == 0){
		pdf_weights->resize(pdf_index+1,std::vector<double>());
	}

	(*pdf_weights)[pdf_index].clear();
     	std::vector<double> weights = (*weightHandle);
	(*pdf_weights)[pdf_index] = weights;
}
