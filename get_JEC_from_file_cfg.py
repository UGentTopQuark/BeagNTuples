import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("CondCore.DBCommon.CondDBCommon_cfi")
from CondCore.DBCommon.CondDBSetup_cfi import *
process.jec = cms.ESSource("PoolDBESSource",
      DBParameters = cms.PSet(
        messageLevel = cms.untracked.int32(0)
        ),
      timetype = cms.string('runnumber'),
      toGet = cms.VPSet(
      cms.PSet(
            record = cms.string('JetCorrectionsRecord'),
            #tag    = cms.string('JetCorrectorParametersCollection_Summer12_V3_DATA_AK5PF'),
             tag    = cms.string('JetCorrectorParametersCollection_Summer12_V3_MC_AK5PF'),
            label  = cms.untracked.string('AK5PFchs')
            ),
      #..................................................
      ## here you add as many jet types as you need
      ## note that the tag name is specific for the particular sqlite file 
      ), 
     # connect = cms.string('sqlite:Summer12_V3_DATA.db')
     # uncomment above tag lines and this comment to use MC JEC
      connect = cms.string('sqlite:Summer12_V3_MC.db')
)
## add an es_prefer statement to resolve a possible conflict from simultaneous connection to a global tag
process.es_prefer_jec = cms.ESPrefer('PoolDBESSource','jec')

process.load('Configuration.StandardSequences.Services_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
#process.GlobalTag.globaltag = 'GR_R_42_V23::All'
process.GlobalTag.globaltag = 'START52_V9::All'
process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(1))
process.source = cms.Source('EmptySource')
process.demo = cms.EDAnalyzer('JetCorrectorDBReader',
  payloadName = cms.untracked.string('AK5PFchs'),
  #globalTag = cms.untracked.string('START42_V23'),
  #globalTag = cms.untracked.string('GR_R_42_V23'),
 # globalTag = cms.untracked.string('START52_V9'),
  #globalTag = cms.untracked.string('SQL_Jet_Corrections_DATA'),
	### this is just an identifier for the txt file name
  globalTag = cms.untracked.string('SQL_Jet_Corrections_MC'),
  printScreen = cms.untracked.bool(False),
  createTextFile = cms.untracked.bool(True),
  useCondDB = cms.untracked.bool(True)
)
process.p = cms.Path(process.demo)
